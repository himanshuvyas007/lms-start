<?php     
    require_once('DbConnect.php');    
	class Login
	{
        protected $dbConn;
	
		public function __construct() {			
			$db = new DbConnect();
			$this->dbConn = $db->connect();
        }
        
        public function login($email,$password) {						
			$stmt = $this->dbConn->prepare("SELECT * FROM users WHERE email=:email AND password=:password");
			$stmt->bindValue(':email', $email);	
			$stmt->bindValue(':password', $password);	
			$stmt->execute();
			$user = $stmt->fetch();						
            if(is_array($user)) {								
				return $user;
			} else {
				return false;
			}				
		}
	}
 ?>