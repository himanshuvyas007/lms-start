<?php
session_start();
include_once dirname(__FILE__) . '/../../lib/common-admin.php';
if(isset($_REQUEST) && !empty($_REQUEST))
{
    // update delete flag in database for soft deletion
    try {
        $delete_user = "update users set is_active = '".$_REQUEST['status']."' where id ='".$_REQUEST['id']."'";
        $delete_user_query = mysqli_query($dbConnection, $delete_user);
        // redirect to listing page with success
        $_SESSION['success'] = "User has been ".(($_REQUEST['status'] == 1) ? "activated" : "deactivated")." successfully";
        redirect(BASE_ADMIN_URL.'/users/lists.php?page='.$_REQUEST['page']);
    } catch(\Exception $e) {
        // redirect to listing page with exception
        $_SESSION['error'] = $e->getMessage();
        redirect(BASE_ADMIN_URL.'/users/lists.php?page='.$_REQUEST['page']);
    }
}
