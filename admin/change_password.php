<?php include_once 'header.php'; ?>
<script src="<?php echo BASE_ADMIN_URL ?>/js/change_password.js"></script>
<div class="page-header">
    <h3 class="page-title">
        Change Password
    </h3>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Change Password</li>
        </ol>
    </nav>
</div>
<div class="row">
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <?php
                if(isset($_POST) && !empty($_POST)) {
                    //check old password is correct or not
                    $check_old_password = "select id from users where password = '" . md5($_POST['old_password']) . "' and id = '" . $_SESSION['active_user_id'] . "' ";
                    $check_old_password_query = mysqli_query($dbConnection, $check_old_password);
                    $check_old_password_array = mysqli_fetch_array($check_old_password_query);
                    if(!empty($check_old_password_array)) {
                        $update_password = "update users set password ='" . md5($_POST['new_password']) . "' where id = '" . $_SESSION['active_user_id'] . "'";
                        $update_password_query = mysqli_query($dbConnection, $update_password);
                        $_SESSION['success'] = "Your password has been changed successfully";
                        redirect(BASE_ADMIN_URL . '/change_password.php');
                    }
                    else {
                        $_SESSION['error'] = "You have entered wrong old password";
                        redirect(BASE_ADMIN_URL . '/change_password.php');
                    }
                }
                ?>
                <?php include_once '../session_message.php'; ?>
                <h4 class="card-title">Change Password</h4>
                <form class="forms-sample" method="post" action="<?php echo BASE_ADMIN_URL . '/change_password.php' ?>">
                    <div class="form-group">
                        <label for="old_password">Old Password</label>
                        <input type="password" class="form-control" id="old_password" name="old_password">
                        <div class="error_message" id="error_old_password"></div>
                    </div>
                    <div class="form-group">
                        <label for="new_password">New Password</label>
                        <input type="password" class="form-control" id="new_password" name="new_password">
                        <div class="error_message" id="error_new_password"></div>
                    </div>
                    <div class="form-group">
                        <label for="confirm_password">Confirm Password</label>
                        <input type="password" class="form-control" id="confirm_password" name="confirm_password">
                        <div class="error_message" id="error_confirm_password"></div>
                    </div>
                    <button type="submit" class="btn btn-gradient-primary mr-2">Submit</button>
                    <button class="btn btn-light">Cancel</button>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
<?php include_once 'footer.php'; ?>