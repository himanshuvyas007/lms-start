$(document).ready(function() {
    $("button[type=submit]").on("click", function() {
        if($.trim($("#page_title").val()) == "")
        {
            $("#error_page_title").html("Please enter page title");
            $("#page_title").focus();
            return false;
        } else {
            $("#error_page_title").html("");
        }
    });
})