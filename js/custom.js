new WOW().init();

$('.testiSlide').slick({
  dots: false,
  infinite: true,
  speed: 300,
  slidesToShow: 3,
  slidesToScroll: 3,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        infinite: true,
        dots: false
      }
    },
	{
      breakpoint: 812,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});

/*input form*/

$('input').focus(function(){
  $(this).parents('.form-group').addClass('focused');
});

$('input').blur(function(){
  var inputValue = $(this).val();
  if ( inputValue == "" ) {
    $(this).removeClass('filled');
    $(this).parents('.form-group').removeClass('focused');  
  } else {
    $(this).addClass('filled');
  }
})  


$('select').click(function(){
  $(this).parents('.form-group').addClass('focused');
});

$('select').blur(function(){
  var inputValue = $(this).val();
  if ( inputValue == "" ) {
    $(this).removeClass('filled');
    $(this).parents('.form-group').removeClass('focused');  
  } else {
    $(this).addClass('filled');
  }
})  

$(document).ready(function(){
    $('.emploRadio input[type="radio"]').click(function(){
        var inputValue = $(this).attr("value");
        var targetBox = $("." + inputValue);
        $(".box").not(targetBox).hide();
        $(targetBox).show();
    });
});


$(document).ready(function() {
	var fixed_nav = $(".nav_top");
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();

        if (scroll >= 100) {
            fixed_nav.addClass("naviBg");
        } else {
            fixed_nav.removeClass("naviBg");
        }
    });
	
	});


$(document).ready(function() {
	var fixed_nav = $(".nav_fix1");
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();

        if (scroll >= 400) {
            fixed_nav.addClass("bg_black_nav1");
        } else {
            fixed_nav.removeClass("bg_black_nav1");
        }
    });
	
	});
	
 
 
 $('.bookLib').on('click', function(){
    if($('.mediDetaliMbo').hasClass('no_highlight')){
        $('.mediDetaliMbo').removeClass('no_highlight');
    }else{
        $('.mediDetaliMbo').addClass('no_highlight');
    }
});  


 $('.bookLib').on('click', function(){
    if($('.bookLibDark').hasClass('no_highlight')){
        $('.bookLibDark').removeClass('no_highlight');
    }else{
        $('.bookLibDark').addClass('no_highlight');
    }
}); 

$("li.dropdown").on("mouseover", function() {
    $(this).children('ul').addClass('dropdownopen');
});