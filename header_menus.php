   <!-- Login -->
   <div class="modal fade" id="logIn" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="logInBox fw">
                        <div class="logInBoxL"> <img class="logIm" src="images/signLogo.png"> <img class="logBo" src="images/signInImg.png"> </div>
                        <div class="logInBoxR"> <span class="close" data-dismiss="modal" aria-label="Close">&times;</span>
                            <h2 class="textHed">Sign In</h2>
                            <div id="afterSubmitErrors"></div>
                            <form id="LoginFormHeader" name="LoginFormHeader">
                                <div class="row">
                                    <div class="col-md-12 logFrom">
                                        <input name="email" placeholder="E-mail" type="text" id="email">
                                        <em>
                                            <div class="icon01"></div>
                                        </em>
                                    </div>
                                    <div class="col-md-12 logFrom">
                                        <input name="password" placeholder="Password" type="password" id="password">
                                        <em>
                                            <div class="icon04"></div>
                                        </em> 
                                    </div>
                                    <div class="col-md-12 logFrom">
                                        <p>
                                            <input name="remember_me" type="checkbox" value="1" id="remember_me">
                                            Remember me</p>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#forgotPassword" id="forgotPasswordLinkLogin">Forgot Password?</a>
                                    </div>
                                    <div class="col-md-12 logFrom">
                                        <input name="SIGN IN" value="SIGN IN" class="formBtn" type="button" id="loginFormButton">
                                        <div id="spinner_gif"></div>
                                    </div>
                                    <div class="col-md-12 logFrom"> <span>Don't have an account? <a href="javascript:void(0)" data-toggle="modal" data-target="#signUp" id="registerPageLinkLogin">Create</a></span> </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Sign Up -->
    <div class="modal fade" id="signUp" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="logInBox fw">
                        <div class="logInBoxL"> <img class="logIm" src="images/signLogo.png"> <img class="logBo" src="images/signInImg.png"> </div>
                        <div class="logInBoxR"> <span class="close" data-dismiss="modal" aria-label="Close">&times;</span>
                            <h2 class="textHed">Sign Up</h2>
                            <div id="afterSubmitErrorsRegister"></div>
                            <form id="RegisterFormHeader" name="RegisterFormHeader">
                                <div class="row">
                                    <div class="col-md-6 logFrom">
                                        <input name="first_name" placeholder="First Name" type="text" id="first_name" maxlength="100">
                                        <em>
                                            <div class="icon01"></div>
                                        </em> </div>
                                    <div class="col-md-6 logFrom">
                                        <input name="last_name" placeholder="Last Name" type="text" id="last_name" maxlength="100">
                                        <em>
                                            <div class="icon01"></div>
                                        </em> </div>
                                    <div class="col-md-6 logFrom">
                                        <input name="phone" placeholder="Mobile No" type="text" id="phone" maxlength="15">
                                        <em>
                                            <div class="icon02"></div>
                                        </em> </div>
                                    <div class="col-md-6 logFrom">
                                        <input name="email" placeholder="E-mail" type="email" id="email_address" maxlength="100">
                                        <em>
                                            <div class="icon03"></div>
                                        </em> </div>
                                    <div class="col-md-6 logFrom">
                                        <input name="password" placeholder="Password" type="password" id="signup_password" maxlength="100">
                                        <em>
                                            <div class="icon04"></div>
                                        </em> </div>
                                    <div class="col-md-6 logFrom">
                                        <input name="confirm_password" placeholder="Confirm Password" type="password" id="confirm_password" maxlength="100">
                                        <em>
                                            <div class="icon04"></div>
                                        </em> </div>
                                    <div class="col-md-12 logFrom">
                                        <input name="SIGN UP" value="SIGN UP" class="formBtn" type="button" id="registerFormButton">
                                        <div id="spinner_gif_register"></div>
                                    </div>
                                    <div class="col-md-12 logFrom"> <span>Already a Member? <a href="javascript:void(0)" data-toggle="modal" data-target="#logIn" id="loginRegisterPageLink">Sign in</a></span> </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Forgot Password -->
    <div class="modal fade" id="forgotPassword" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="logInBox fw">
                        <div class="logInBoxL"> <img class="logIm" src="images/signLogo.png"> <img class="logBo" src="images/signInImg.png"> </div>
                        <div class="logInBoxR"> <span class="close" data-dismiss="modal" aria-label="Close">&times;</span>
                            <h2 class="textHed">Forgot Password</h2>
                            <div id="afterSubmitErrorsForgot"></div>
                            <form id="ForgotFormHeader" name="ForgotFormHeader">
                                <div class="row">
                                    <div class="col-md-12 logFrom">
                                        <input name="email" placeholder="E-mail" type="email" id="email_address_forgot" maxlength="100">
                                        <em>
                                            <div class="icon03"></div>
                                        </em> 
                                    </div>
                                    <div class="col-md-12 logFrom">
                                        <input name="Forgot" value="Submit" class="formBtn" type="button" id="ForgotFormFormButton">
                                        <div id="spinner_gif_forgot"></div>
                                    </div>
                                    <div class="col-md-12 logFrom"><a href="javascript:void(0)" data-toggle="modal" data-target="#logIn" id="loginRegisterPageLinkForgot">Sign in</a></span> </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <header class="naviMain innerHead nav_top">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <nav class="navbar navbar-expand-lg"> <a class="navbar-brand" href="<?php echo BASE_URL; ?>"><img src="images/logo.png"></a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item active"> <a class="nav-link" href="<?php echo BASE_URL; ?>">Home</a> </li>
                                <li class="nav-item"> <a class="nav-link" href="#">Members</a> </li>
                                <li class="nav-item"> <a class="nav-link" href="#">Chat Rooms</a> </li>
                                <?php if (!isset($_SESSION['activeUserIdFront']) || $_SESSION['activeUserIdFront'] == "") { ?>
                                    <li class="nav-item"> <a class="nav-link login" data-toggle="modal" data-target="#logIn" href="javascript:void(0);">Login</a> </li>
                                    <li class="nav-item"> <a class="nav-link signUp" data-toggle="modal" data-target="#signUp" href="javascript:void(0);">Sign Up</a> </li>
                                <?php } else { ?>
                                    <li class="nav-item dropdown">
                                        <li class="nav-item"> <a class="nav-link" href="<?php echo BASE_URL.'profile.php'; ?>">My Profile</a> </li>
                                        <li class="nav-item"> <a class="nav-link" href="<?php echo BASE_URL.'change_password.php'; ?>">Change Password</a> </li>
                                    </li>
                                    <li class="nav-item"> <a class="nav-link login" href="<?php echo BASE_URL . 'logout.php'; ?>">Logout</a> </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>