$(document).ready(function() {
    $("button[type=submit]").on("click", function() {
        if($.trim($("#old_password").val()) == "") 
        {
            $("#error_old_password").html("Please enter your old password");
            $("#old_password").focus();
            return false;
        } else {
            $("#error_old_password").html("");
        }
        if($.trim($("#new_password").val()) == "") 
        {
            $("#error_new_password").html("Please enter your new password");
            $("#new_password").focus();
            return false;
        } else {
            $("#error_new_password").html("");
        }
        if($.trim($("#confirm_password").val()) == "") 
        {
            $("#error_confirm_password").html("Please confirm your password");
            $("#confirm_password").focus();
            return false;
        } else {
            $("#error_confirm_password").html("");
        }
        if($.trim($("#confirm_password").val()) != $.trim($("#new_password").val())) 
        {
            $("#error_confirm_password").html("Passwords do not match");
            $("#confirm_password").focus();
            return false;
        } else {
            $("#error_confirm_password").html("");
        }
    });
})