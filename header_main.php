<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->

    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="css/styles.css" rel="stylesheet" type="text/css">
    <link href="css/custom.css" rel="stylesheet" type="text/css">
    <link href="css/slick.css" rel="stylesheet" type="text/css">
    <link href="css/responsive.css" rel="stylesheet" type="text/css">
    <link href="css/animate.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?php echo BASE_URL.'js/alertifyjs/css/alertify.min.css'?>">
    <script type="text/javascript"> var BASE_URL = '<?php echo BASE_URL; ?>' </script>
    <title>Websibis</title>
</head>

<body>
<?php include_once 'header_menus.php'; ?>
<div class="maindiv">
	<div class="homeSlide innerHead1">
  <div class="container fadeInUp wow">
    <div class="row">
      <div class="col-12 col-md-6">
      	<div class="innerSearch">
        	<input name="" placeholder="Search Members (3 to 40 character)" type="text">
            <input name="" type="submit">
        </div>
      </div>
      <div class="col-12 col-md-6">
      	<div class="couterdiv couterdiv1" style="visibility: visible; animation-name: slideInDown;">
            <ul>
              <li>1</li>
              <li>2</li>
              <li>2</li>
              <li>4</li>
              <li>7</li>
              <li>9</li>
            </ul>
            <p>Members Online Now</p>
          </div>
      </div>
    </div>
  </div>
</div>