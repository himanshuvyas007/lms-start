$(document).ready(() => {
    var messageCounter = 0;
    $.ajax({
        url: 'https://randomuser.me/api/?results=10&inc=name,picture&noinfo',
        dataType: 'json',
        success: function (data) {
            data.results.map(item => {
                let personRow = `
                <li class="cursor-pointer">
                    <div class="row align-items-center pt-3 pb-3">
                        <div class="col-3 text-center">
                        <span class="badge unread"><span class="count">' + Math.floor(Math.random() * 90 + 10) + '</span></span>
                            <img src="https://www.cumbria.ac.uk/media/staff-profile-images/staff_profile_-generic_350x350px.png" alt="Person" class="w-100 rounded-circle">
                        </div>
                        <div class="col pl-0 text-truncate">
                            <h5 class="ft-poppins">{{name}}</h5>
                            <p class="m-0 text-muted small">
                                {{lastMessage}}
                            </p>
                        </div>
                    </div>
                </li>
                `

                // if (messageCounter == 0 || messageCounter == 1 || messageCounter == 3 || messageCounter == 5)
                //     personRow = personRow.replace("{{unreadMessage}}", '<span class="badge unread"><span class="count">' + Math.floor(Math.random() * 90 + 10) + '</span></span>');
                // else
                //     personRow = personRow.replace("{{unreadMessage}}", "");

                // personRow = personRow.replace("{{dp}}", item.picture.large).replace("{{name}}", item.name.first.charAt(0).toUpperCase() + item.name.first.slice(1) + " " + item.name.last.charAt(0).toUpperCase() + item.name.last.slice(1)).replace("{{lastMessage}}", messages[messageCounter]);

                // messageCounter++;

                // $("#people-list").append(personRow);
            });
        }
    });

    // $("#people-list").click(() => {
    //     $("#nameHeaderRow").css("display", "flex");
    //     $("#yourChatsHeading").css("display", "none");
    //     $(".chat-column").show();
    //     $(".sidebar").hide();
    // });

    $("#back-button").click(() => {
        $("#nameHeaderRow").css("display", "none");
        $("#yourChatsHeading").css("display", "flex");
        $(".chat-column").hide();
        $(".sidebar").show();
    });

    $("#video-call-button").click(() => {
        $("#video-call-screen").fadeIn();

        var countVideoCallCounter = 0;
        window.countVideoCall = setInterval(function () {
            countVideoCallCounter++;
            document.getElementById('calling-animation').innerHTML = "Calling Dhruvik." + new Array(countVideoCallCounter % 10).join('.');
        }, 1000);

        setTimeout(() => {
            $("#calling").fadeOut();
            clearInterval(window.countVideoCall);
            document.getElementById('calling-animation').innerHTML = "Calling Dhruvik";
            $("#on-call").fadeIn();
        }, 3000);
    });

    $(".end-call").click(() => {
        $("#video-call-screen").fadeOut();
        $("#on-call").hide();
        $("#calling").show();
        clearInterval(window.countVideoCall);
        document.getElementById('calling-animation').innerHTML = "Calling Dhruvik";
    });
});