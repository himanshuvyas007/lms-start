<?php include_once dirname(__FILE__) . '/../header.php'; ?>
<script type="text/javascript" src="<?php echo BASE_ADMIN_URL.'/js/file-upload.js'; ?>"></script>
<script type="text/javascript" src="<?php echo BASE_ADMIN_URL.'/js/settings/form.js'; ?>"></script>
<div class="page-header">
    <h3 class="page-title">
        Site Settings
    </h3>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Site Settings</li>
        </ol>
    </nav>
</div>
<?php if(isset($validation_array['global']) && !empty($validation_array['global'])) { ?>
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card text-center">
            <div class="alert alert-danger"><?php echo $validation_array['global']; ?></div>
        </div>
    </div>
<?php } ?>
<div class="row">
    <?php 

    // get settings details
    $check_settings = "select * from settings";
    $check_settings_query = mysqli_query($dbConnection, $check_settings);
    $check_settings_query_array = mysqli_fetch_array($check_settings_query);

    if(isset($_POST) && !empty($_POST))
    {
        // save data
        $validation_array = [];
        try {            
            // check image is uploaded or not
            $site_logo = "";
            $favicon = "";
            
            if(isset($_FILES['site_logo']) && !empty($_FILES['site_logo']) && isset($_FILES['site_logo']['name']) && $_FILES['site_logo']['name'] != "")
            {
                // check valid extensions of image
                $image_extension = pathinfo($_FILES['site_logo']['name'], PATHINFO_EXTENSION);
                if(in_array($image_extension, ALLOWED_IMAGE_EXTENSIONS))
                {
                    $imageSize = getimagesize($_FILES['site_logo']['tmp_name']);
                    $imageWidth = $imageSize[0];
                    $imageHeight = $imageSize[1];
                    if($imageWidth > 300 || $imageHeight > 300) {
                        $validation_array['site_logo'] = "Site logo size should be not be greater than 300X300 pixels";
                    } else {
                        $site_logo = md5(time()).'.'.$image_extension;
                        move_uploaded_file($_FILES['site_logo']['tmp_name'], BASE_PATH.'/uploads/settings/'.$site_logo);
                    }
                } else {
                    $validation_array['site_logo'] = "Uploaded site logo should be of ".implode(", ", ALLOWED_IMAGE_EXTENSIONS)." type only ";
                }
            }
            if(isset($_FILES['favicon']) && !empty($_FILES['favicon']) && isset($_FILES['favicon']['name']) && $_FILES['favicon']['name'] != "")
            {
                // check valid extensions of image
                $favicon_extension = pathinfo($_FILES['favicon']['name'], PATHINFO_EXTENSION);
                if(in_array($favicon_extension, ALLOWED_FAVICON_EXTENSIONS))
                {
                    // validate image width and height
                    $imageSize = getimagesize($_FILES['favicon']['tmp_name']);
                    $imageWidth = $imageSize[0];
                    $imageHeight = $imageSize[1];
                    if($imageWidth > 32 || $imageHeight > 32) {
                        $validation_array['favicon'] = "Favicon icon size should be not be greater than 32X32 pixels";
                    } else {
                        $favicon = md5(time()).'.'.$favicon_extension;
                        move_uploaded_file($_FILES['favicon']['tmp_name'], BASE_PATH.'/uploads/settings/'.$favicon);
                    }
                } else {
                    $validation_array['favicon'] = "Favicon icon should be of ".implode(", ", ALLOWED_IMAGE_EXTENSIONS)." type only ";
                }
            }

            // validate all required fields

            if(!isset($_POST['site_title']) || $_POST['site_title'] == "")
                $validation_array['site_title'] = "Please enter site title";

            if(!isset($_POST['site_email']) || $_POST['site_email'] == "")
                $validation_array['site_email'] = "Please enter site email";
            
            if(!preg_match("/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/", $_POST['site_email']))
                $validation_array['site_email'] = "Please enter valid site email";

            if($_POST['contact_email'] != "" && !preg_match("/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/", $_POST['contact_email']))
                $validation_array['contact_email'] = "Please enter valid site email";

            if($_POST['smtp_port'] != "" && !is_numeric($_POST['smtp_port']))
                $validation_array['smtp_port'] = "Please enter only numeric values";

            if($_POST['rows_per_page'] != "" && !is_numeric($_POST['rows_per_page']))
                $validation_array['rows_per_page'] = "Please enter only numeric values";

            if(!isset($validation_array) || empty($validation_array)) 
            {
                // check that settings are already exist or not
                $check_settings = "select id from settings";
                $check_settings_query = mysqli_query($dbConnection, $check_settings);
                $check_settings_query_array = mysqli_fetch_array($check_settings_query);
                if(isset($check_settings_query_array['id'])) {
                    $save_settings = "update settings set rows_per_page='".$_POST['rows_per_page']."',site_title='".$_POST['site_title']."',site_email='".$_POST['site_email']."',contact_email='".$_POST['contact_email']."',
                                       smtp_host='".$_POST['smtp_host']."',smtp_username='".$_POST['smtp_username']."',smtp_password='".$_POST['smtp_password']."',smtp_port='".$_POST['smtp_port']."' ";
                    if($site_logo != "") 
                    {
                        $save_settings .= ", site_logo='".$site_logo."' ";
                    }

                    if($favicon != "") 
                    {
                        $save_settings .= ", favicon='".$favicon."' ";
                    }

                    $save_settings .= ", modified='".date("Y-m-d H:i:s")."' where id = '".$check_settings_query_array['id']."' ";

                    $save_user_query = mysqli_query($dbConnection, $save_settings);                  
                } else {
                    $save_settings = "insert into settings set rows_per_page='".$_POST['rows_per_page']."',site_title='".$_POST['site_title']."',site_email='".$_POST['site_email']."',contact_email='".$_POST['contact_email']."',
                    smtp_host='".$_POST['smtp_host']."',smtp_username='".$_POST['smtp_username']."',smtp_password='".$_POST['smtp_password']."',smtp_port='".$_POST['smtp_port']."'";

                    if($site_logo != "") 
                    {
                        $save_settings .= ", site_logo='".$site_logo."' ";
                    }

                    if($favicon != "") 
                    {
                        $save_settings .= ", favicon='".$favicon."' ";
                    }

                    $save_settings .= ", created = '".date("Y-m-d H:i:s")."', modified='".date("Y-m-d H:i:s")."' ";
                    $save_user_query = mysqli_query($dbConnection, $save_settings);
                }
                $_SESSION['success'] = "Settings has been saved successfully";
                redirect(BASE_ADMIN_URL.'/settings/form.php');
            }
            
        } catch (\Exception $e) {
            $validation_array['global'] = $e->getMessage();
        }
    }
    ?>
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <form class="forms-sample" action="" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="site_title">Site Title</label>
                        <input type="text" class="form-control" id="site_title" maxlength="100" name="site_title" value="<?php echo ((isset($_POST['site_title']) && $_POST['site_title'] != "") ? $_POST['site_title'] : $check_settings_query_array['site_title']); ?>">
                        <div class="error_message" id="error_site_title"><?php echo ((isset($validation_array['site_title']) && !empty($validation_array['site_title'])) ? $validation_array['site_title'] : "");?></div>
                    </div>
                    <div class="form-group">
                        <label for="site_email">Site Email</label>
                        <input type="text" class="form-control" id="site_email" maxlength="100" name="site_email" value="<?php echo ((isset($_POST['site_email']) && $_POST['site_email'] != "") ? $_POST['site_email'] : $check_settings_query_array['site_email']); ?>">
                        <div class="error_message" id="error_site_email"><?php echo ((isset($validation_array['site_email']) && !empty($validation_array['site_email'])) ? $validation_array['site_email'] : "");?></div>
                    </div>
                    <div class="form-group">
                        <label for="contact_email">Contact Email</label>
                        <input type="email" class="form-control" id="contact_email" name="contact_email" value="<?php echo ((isset($_POST['contact_email']) && $_POST['contact_email'] != "") ? $_POST['contact_email'] : $check_settings_query_array['contact_email']); ?>">
                        <div class="error_message" id="error_contact_email"><?php echo ((isset($validation_array['contact_email']) && !empty($validation_array['contact_email'])) ? $validation_array['contact_email'] : "");?></div>
                    </div>
                    <div class="form-group">
                        <label for="smtp_host">SMTP Host</label>
                        <input type="text" class="form-control" id="smtp_host" name="smtp_host" value="<?php echo ((isset($_POST['smtp_host']) && $_POST['smtp_host'] != "") ? $_POST['smtp_host'] : ""); ?>">
                        <div class="error_message" id="error_smtp_host"><?php echo ((isset($validation_array['smtp_host']) && !empty($validation_array['smtp_host'])) ? $validation_array['smtp_host'] : "");?></div>
                    </div>
                    <div class="form-group">
                        <label for="smtp_username">SMTP Username</label>
                        <input type="text" class="form-control" id="smtp_username" name="smtp_username" value="<?php echo ((isset($_POST['smtp_username']) && $_POST['smtp_username'] != "") ? $_POST['smtp_username'] : $check_settings_query_array['smtp_username']); ?>">
                        <div class="error_message" id="error_smtp_username"><?php echo ((isset($validation_array['smtp_username']) && !empty($validation_array['smtp_username'])) ? $validation_array['smtp_username'] : "");?></div>
                    </div>
                    <div class="form-group">
                        <label for="smtp_password">SMTP Password</label>
                        <input type="password" class="form-control" id="smtp_password" name="smtp_password" value="<?php echo ((isset($_POST['smtp_password']) && $_POST['smtp_password'] != "") ? $_POST['smtp_password'] : $check_settings_query_array['smtp_password']); ?>">
                    </div>
                    <div class="form-group">
                        <label for="smtp_port">SMTP Port</label>
                        <input type="text" class="form-control" id="smtp_port" name="smtp_port" value="<?php echo ((isset($_POST['smtp_port']) && $_POST['smtp_port'] != "") ? $_POST['smtp_port'] : $check_settings_query_array['smtp_port']); ?>">
                        <div class="error_message" id="error_smtp_port"><?php echo ((isset($validation_array['smtp_port']) && !empty($validation_array['smtp_port'])) ? $validation_array['smtp_port'] : "");?></div>
                    </div>
                    <div class="form-group">
                        <label for="rows_per_page">Records per page</label>
                        <input type="text" class="form-control" id="rows_per_page" name="rows_per_page" value="<?php echo ((isset($_POST['rows_per_page']) && $_POST['rows_per_page'] != "") ? $_POST['rows_per_page'] : $check_settings_query_array['rows_per_page']); ?>">
                        <div class="error_message" id="error_rows_per_page"><?php echo ((isset($validation_array['rows_per_page']) && !empty($validation_array['rows_per_page'])) ? $validation_array['rows_per_page'] : "");?></div>
                    </div>
                    <div class="form-group">
                        <label>Site Logo</label>
                        <input type="file" name="site_logo" class="file-upload-default">
                        <div class="input-group col-xs-12">
                            <input type="text" class="form-control file-upload-info" disabled>
                            <span class="input-group-append">
                                <button class="file-upload-browse btn btn-gradient-primary" type="button">Upload</button>
                            </span>
                        </div><br/>
                        <?php if(isset($check_settings_query_array['site_logo']) && $check_settings_query_array['site_logo'] != "" && file_exists(BASE_PATH.'/uploads/settings/'.$check_settings_query_array['site_logo'])) { ?>
                            <img src="<?php echo BASE_URL.'/uploads/settings/'.$check_settings_query_array['site_logo']; ?>" width="100">
                        <?php } else { ?>
                            <img src="<?php echo BASE_ADMIN_URL.'/img/no-available-image.png'; ?>" width="100">
                        <?php }?>
                        <div class="error_message">
                            <i>Site logo size should be not be greater than 100X100 pixels</i>
                        </div>
                        <div class="error_message">
                            <?php echo ((isset($validation_array['site_logo']) && !empty($validation_array['site_logo'])) ? $validation_array['site_logo'] : "");?>
                        </div>
                    </div> 
                    <div class="form-group">
                        <label>Favicon Icon</label>
                        <input type="file" name="favicon" class="file-upload-default">
                        <div class="input-group col-xs-12">
                            <input type="text" class="form-control file-upload-info" disabled>
                            <span class="input-group-append">
                                <button class="file-upload-browse btn btn-gradient-primary" type="button">Upload</button>
                            </span>
                        </div><br/>
                        <?php if(isset($check_settings_query_array['favicon']) && $check_settings_query_array['favicon'] != "" && file_exists(BASE_PATH.'/uploads/settings/'.$check_settings_query_array['favicon'])) { ?>
                            <img src="<?php echo BASE_URL.'/uploads/settings/'.$check_settings_query_array['favicon']; ?>" width="32">
                        <?php } else { ?>
                            <img src="<?php echo BASE_ADMIN_URL.'/img/no-available-image.png'; ?>" width="32">
                        <?php }?>
                        <div class="error_message">
                            <i>Favicon icon size should be not be greater than 32X32 pixels</i>
                        </div>
                        <div class="error_message">
                            <?php echo ((isset($validation_array['favicon']) && !empty($validation_array['favicon'])) ? $validation_array['favicon'] : "");?>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-gradient-primary mr-2">Save Settings</button>
                    <a href="<?php echo BASE_ADMIN_URL.'/users/lists.php'; ?>" class="btn btn-light">Cancel</a>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
<?php include_once dirname(__FILE__) . '/../footer.php'; ?>