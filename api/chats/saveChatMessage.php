<?php 
session_start();
include_once dirname(__FILE__) .'/../../lib/common-admin.php';
//  API to create one to one communication channel
try {
    $data = [];
    if(isset($_POST) && !empty($_POST)) {
        $_SESSION['activeUserIdFront'] = 4;
        $contactId = $_POST['contactTo'];
        $randomChatRoomName = base64_encode(uniqid());
        $createChannelQuery = "insert into chat_rooms set room_name = '".$randomChatRoomName."', room_owner = '".$_SESSION['activeUserIdFront']."', is_active = ".true.", 
                               type = '".ONE_TO_ONE_CHAT_ROOM."', created = '".date("Y-m-d H:i:s")."', modified = '".date("Y-m-d H:i:s")."' ";
        $createChannelQueryResult = mysqli_query($dbConnection, $createChannelQuery);
        $chatRoomId = mysqli_insert_id($dbConnection);

        // add chat room member records
        $userIdArray = [$_SESSION['activeUserIdFront'], $contactId];
        foreach ($userIdArray as $userId) {
            $createChatRoomMembers = "insert into chat_room_members set room_id = '".$chatRoomId."', user_id = '".$userId."'";
            if($userId === $_SESSION['activeUserIdFront']) {
                $createChatRoomMembers .= ", is_user_active = 1";
            } else {
                $createChatRoomMembers .= ", is_user_active = 0";
            }
            $createChatRoomMembers .= ", created = '".date("Y-m-d H:i:s")."', modified = '".date("Y-m-d H:i:s")."' ";
            $createChatRoomMembersResult = mysqli_query($dbConnection, $createChatRoomMembers);
        }
        $meta['responseCode'] = 200;
    } else {
        throw new \Exception("Request data is not empty");
    }
} catch (\Exception $e) {
    $meta['responseCode'] = 400;
}
echo json_encode($meta);
?>