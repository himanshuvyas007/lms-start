-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.26 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for lms_video
CREATE DATABASE IF NOT EXISTS `lms_video` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `lms_video`;

-- Dumping structure for table lms_video.chats
CREATE TABLE IF NOT EXISTS `chats` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `room_id` int(11) DEFAULT NULL,
  `messages` longtext,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table lms_video.chats: 2 rows
/*!40000 ALTER TABLE `chats` DISABLE KEYS */;
INSERT INTO `chats` (`id`, `room_id`, `messages`, `created`, `modified`) VALUES
	(1, 1, '[{"userId":"NA==#10101#","message":"","file":{"originalFileName":"lms_video.sql","filePath":"/uploads/chatsUpload/9f605be3c876d5c54836a453f0803d0b.sql"},"date":"2020-01-26 13:37:48"}]', '2020-01-26 13:37:09', '2020-01-26 13:37:48'),
	(2, 2, '', '2020-01-26 13:37:24', '2020-01-26 13:37:24');
/*!40000 ALTER TABLE `chats` ENABLE KEYS */;

-- Dumping structure for table lms_video.chat_rooms
CREATE TABLE IF NOT EXISTS `chat_rooms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_name` varchar(255) DEFAULT NULL,
  `room_owner` int(11) DEFAULT NULL,
  `members` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `type` tinyint(1) DEFAULT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table lms_video.chat_rooms: 2 rows
/*!40000 ALTER TABLE `chat_rooms` DISABLE KEYS */;
INSERT INTO `chat_rooms` (`id`, `room_name`, `room_owner`, `members`, `is_active`, `type`, `created`, `modified`) VALUES
	(1, 'NWUyZDk2MDU5N2QyZA==', 4, '4,5', 1, 0, '2020-01-26 13:37:09', '2020-01-26 13:37:09'),
	(2, 'great', 4, NULL, 1, 1, '2020-01-26 13:37:24', '2020-01-26 13:37:24');
/*!40000 ALTER TABLE `chat_rooms` ENABLE KEYS */;

-- Dumping structure for table lms_video.chat_room_members
CREATE TABLE IF NOT EXISTS `chat_room_members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `is_user_active` tinyint(1) DEFAULT NULL,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `room_id_user_id` (`room_id`,`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table lms_video.chat_room_members: 4 rows
/*!40000 ALTER TABLE `chat_room_members` DISABLE KEYS */;
INSERT INTO `chat_room_members` (`id`, `room_id`, `user_id`, `is_user_active`, `created`, `modified`) VALUES
	(1, 1, 4, 1, '2020-01-26 13:37:09', '2020-01-26 13:37:09'),
	(2, 1, 5, 1, '2020-01-26 13:37:09', '2020-01-26 13:37:09'),
	(3, 2, 5, 1, '2020-01-26 13:37:24', '2020-01-26 13:37:24'),
	(4, 2, 4, 1, '2020-01-26 13:37:24', '2020-01-26 13:37:24');
/*!40000 ALTER TABLE `chat_room_members` ENABLE KEYS */;

-- Dumping structure for table lms_video.pages
CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `page_title` varchar(255) DEFAULT NULL,
  `content` longtext,
  `meta_title` text,
  `meta_keywords` text,
  `meta_description` longtext,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table lms_video.pages: 0 rows
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;

-- Dumping structure for table lms_video.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `key_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table lms_video.roles: ~3 rows (approximately)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `name`, `key_name`) VALUES
	(1, 'Administrator', 'ROLE_ADMINISTRATOR'),
	(2, 'User 1', 'ROLE_USER_1'),
	(3, 'User 2', 'ROLE_USER_2');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping structure for table lms_video.settings
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rows_per_page` int(5) DEFAULT NULL,
  `site_title` varchar(255) DEFAULT NULL,
  `site_email` varchar(50) DEFAULT NULL,
  `contact_email` varchar(50) DEFAULT NULL,
  `site_logo` varchar(50) DEFAULT NULL,
  `smtp_host` varchar(50) DEFAULT NULL,
  `smtp_username` varchar(100) DEFAULT NULL,
  `smtp_password` varchar(255) DEFAULT NULL,
  `smtp_port` varchar(50) DEFAULT NULL,
  `favicon` varchar(50) DEFAULT NULL,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table lms_video.settings: 1 rows
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` (`id`, `rows_per_page`, `site_title`, `site_email`, `contact_email`, `site_logo`, `smtp_host`, `smtp_username`, `smtp_password`, `smtp_port`, `favicon`, `created`, `modified`) VALUES
	(1, 10, 'LMS Video', 'test@test.com', 'test@test.com', NULL, '', '', '', '', NULL, '2020-01-26 13:26:10', '2020-01-26 13:26:10');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;

-- Dumping structure for table lms_video.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) DEFAULT '2',
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `zipcode` varchar(50) DEFAULT NULL,
  `address_1` tinytext,
  `address_2` tinytext,
  `image` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `is_verified` tinyint(1) NOT NULL DEFAULT '0',
  `forgot_password_key` varchar(255) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  `last_login` timestamp NULL DEFAULT NULL,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `FK_users_roles` (`role_id`),
  CONSTRAINT `FK_users_roles` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table lms_video.users: ~2 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `role_id`, `first_name`, `last_name`, `email`, `password`, `phone`, `country`, `state`, `city`, `zipcode`, `address_1`, `address_2`, `image`, `is_active`, `is_verified`, `forgot_password_key`, `is_deleted`, `last_login`, `created`, `modified`) VALUES
	(1, 1, 'Admin', 'Admin', 'admin@admin.com', 'e10adc3949ba59abbe56e057f20f883e', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, '2020-01-26 13:08:50', '2019-07-12 01:11:01', '2020-01-26 18:38:50'),
	(4, 2, 'Himanshu 1', 'Vyas 1', 'himanshuvyas246@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '7894563210', 'Guatemala', 'Rajasthan', 'Jaipur', '35165468468', 'wefewfwefewfewf awfwadwfwqf', 'wefwegewgfewgwgwr WDWQFQWFWQ', 'b35b13d2fd2ebd2ae0436e1d80861394.jpg', 1, 1, NULL, 0, '2020-01-26 13:09:11', '2019-07-13 15:21:36', '2020-01-26 18:39:11'),
	(5, 2, 'Ankit', 'Raj', 'ankit.raj@gmail.com', '25d55ad283aa400af464c76d713c07ad', '23423432432', 'India', 'efewf ewfe w', 'we fewf ewfew', '12321312', '23432432b32rb 3ew', 's ffd', NULL, 1, 1, NULL, 0, NULL, '2020-01-26 13:17:57', '2020-01-26 13:17:57');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
