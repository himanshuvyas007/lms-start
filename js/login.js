$("#loginFormButton").on("click", function() {
    if($.trim($("#email").val()) == "") 
    {
        alertify.alert("Please enter your email address");
        $("#email").focus();
        return false;
    }
    if($.trim($("#password").val()) == "") 
    {
        alertify.alert("Please enter your password");
        $("#password").focus();
        return false;
    }

    $("#loginFormButton").attr("disabled", "disabled");
    $("#spinner_gif").html('<img src="'+BASE_URL + 'images/loadspinner.gif" width="50" />');
    $.ajax({
        type : "POST",
        url : BASE_URL + 'check_login.php',
        data : {
            email : $("#email").val(),
            password : $("#password").val(),
            remember_me : $("#remember_me").val()
        },
        dataType: 'json',
        success: function(data) {
            if(data.status == 200) {
                $("#spinner_gif").html('');
                window.location.href= BASE_URL;
            } else {
                $("#spinner_gif").html('');
                $("#loginFormButton").removeAttr("disabled");
                $("#afterSubmitErrors").html("<div class='alert alert-danger'>"+data.message+"</div>");
            }
        },
        error : function(error) {
            $("#spinner_gif").html('');
            $("#loginFormButton").removeAttr("disabled");
            $("#afterSubmitErrors").html("<div class='alert alert-danger'>"+eval(error)+"</div>");
        }
    })
});

$("#loginRegisterPageLink").on("click", function() {
    $('#signUp').modal('hide');
    $('#forgotPassword').modal('hide');
});
$("#registerPageLinkLogin").on("click", function() {
    $('#logIn').modal('hide');
    $('#forgotPassword').modal('hide');
});
$("#loginRegisterPageLinkForgot").on("click", function() {
    $('#forgotPassword').modal('hide');
    $('#signUp').modal('hide');
});
$("#forgotPasswordLinkLogin").on("click", function() {
    $('#logIn').modal('hide');
    $('#signUp').modal('hide');
});

$("#registerFormButton").on("click", function() {
    if($.trim($("#first_name").val()) == "") 
    {
        alertify.alert("Please enter your first name");
        $("#first_name").focus();
        return false;
    }
    if($.trim($("#last_name").val()) == "") 
    {
        alertify.alert("Please enter your last name");
        $("#last_name").focus();
        return false;
    }
    if($.trim($("#phone").val()) == "") 
    {
        alertify.alert("Please enter your phone number");
        $("#phone").focus();
        return false;
    }
    if(isNaN($.trim($("#phone").val()))) 
    {
        alertify.alert("Please enter valid phone number");
        $("#phone").focus();
        return false;
    }
    if($.trim($("#email_address").val()) == "") 
    {
        alertify.alert("Please enter your email address");
        $("#email_address").focus();
        return false;
    }
    if($.trim($("#email_address").val()) != "") 
    {
        var mailformat = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if($("#email_address").val().match(mailformat))
        {

        } else {
            alertify.alert("Please enter valid email address!");
            $("#email_address").focus();
            return false;
        }
    }
    if($.trim($("#signup_password").val()) == "") 
    {
        alertify.alert("Please enter your password");
        $("#signup_password").focus();
        return false;
    }
    if($.trim($("#signup_password").val()).length < 8) 
    {
        alertify.alert("Please enter password of minimum 8 characters long");
        $("#signup_password").focus();
        return false;
    }
    if($.trim($("#confirm_password").val()) == "") 
    {
        alertify.alert("Please confirm your password");
        $("#confirm_password").focus();
        return false;
    }
    if($.trim($("#signup_password").val()) != $.trim($("#confirm_password").val())) 
    {
        alertify.alert("Passwords do not match");
        $("#confirm_password").focus();
        return false;
    }

    $("#registerFormButton").attr("disabled", "disabled");
    $("#spinner_gif_register").html('<img src="'+BASE_URL + 'images/loadspinner.gif" width="50" />');
    $.ajax({
        type : "POST",
        url : BASE_URL + 'register_submit.php',
        data : {
            first_name : $("#first_name").val(),
            last_name : $("#last_name").val(),
            phone : $("#phone").val(),
            email : $("#email_address").val(),
            password : $("#signup_password").val(),
            confirm_password : $("#confirm_password").val()
        },
        dataType: 'json',
        success: function(data) {
            if(data.status == 200) {
                $("#spinner_gif_register").html('');
                window.location.href= BASE_URL;
            } else {
                $("#spinner_gif_register").html('');
                $("#registerFormButton").removeAttr("disabled");
                if(Array.isArray(data.message) == false) 
                {
                    $("#afterSubmitErrorsRegister").html("<div class='alert alert-danger'>"+data.message+"</div>");
                } else {
                    let errorMessage = "<div class='alert alert-danger'>";
                    $(data.message).each(function(key, value) {
                        errorMessage = errorMessage + value;
                    });
                    errorMessage = errorMessage +"</div>";
                    $("#afterSubmitErrorsRegister").html(errorMessage);
                }
            }
        },
        error : function(error) {
            $("#spinner_gif_register").html('');
            $("#registerFormButton").removeAttr("disabled");
            $("#afterSubmitErrorsRegister").html("<div class='alert alert-danger'>"+eval(error)+"</div>");
        }
    })
});

$("#ForgotFormFormButton").on("click", function() {
    if($.trim($("#email_address_forgot").val()) == "") 
    {
        alertify.alert("Please enter your email address");
        $("#email_address_forgot").focus();
        return false;
    }
    if($.trim($("#email_address_forgot").val()) != "") 
    {
        var mailformat = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if($("#email_address_forgot").val().match(mailformat))
        {

        } else {
            alertify.alert("Please enter valid email address!");
            $("#email_address_forgot").focus();
            return false;
        }
    }

    $("#ForgotFormFormButton").attr("disabled", "disabled");
    $("#spinner_gif_forgot").html('<img src="'+BASE_URL + 'images/loadspinner.gif" width="50" />');
    $.ajax({
        type : "POST",
        url : BASE_URL + 'forgot_password.php',
        data : {
            email : $("#email_address_forgot").val()
        },
        dataType: 'json',
        success: function(data) {
            if(data.status == 200) {
                $("#spinner_gif_forgot").html('');
                window.location.href= BASE_URL;
            } else {
                $("#spinner_gif_forgot").html('');
                $("#ForgotFormFormButton").removeAttr("disabled");
                if(Array.isArray(data.message) == false) 
                {
                    $("#afterSubmitErrorsForgot").html("<div class='alert alert-danger'>"+data.message+"</div>");
                } else {
                    let errorMessage = "<div class='alert alert-danger'>";
                    $(data.message).each(function(key, value) {
                        errorMessage = errorMessage + value;
                    });
                    errorMessage = errorMessage +"</div>";
                    $("#afterSubmitErrorsForgot").html(errorMessage);
                }
            }
        },
        error : function(error) {
            $("#spinner_gif_forgot").html('');
            $("#ForgotFormFormButton").removeAttr("disabled");
            $("#afterSubmitErrorsForgot").html("<div class='alert alert-danger'>"+eval(error)+"</div>");
        }
    });
});


$("#footer_submit").on('click', function() {
    if($.trim($("#footer_name").val()) == "") 
    {
        alertify.alert("Please enter your full name");
        $("#footer_name").focus();
        return false;
    }
    if($.trim($("#footer_email").val()) == "") 
    {
        alertify.alert("Please enter your email address");
        $("#footer_email").focus();
        return false;
    }
    if($.trim($("#footer_email").val()) != "") 
    {
        var mailformat = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if($("#footer_email").val().match(mailformat))
        {

        } else {
            alertify.alert("Please enter valid email address!");
            $("#footer_email").focus();
            return false;
        }
    }
    if($.trim($("#footer_message").val()) == "") 
    {
        alertify.alert("Please enter your message");
        $("#footer_message").focus();
        return false;
    }
    $("#footer_submit").attr("disabled", "disabled");
    $("#spinner_gif_forgot").html('<img src="'+BASE_URL + 'images/loadspinner.gif" width="50" />');
    $.ajax({
        type : "POST",
        url : BASE_URL + 'enquiry.php',
        data : {
            name: $("#footer_name").val(),
            email : $("#footer_email").val(),
            message : $("#footer_message").val()
        },
        dataType: 'json',
        success: function(data) {
            if(data.status == 200) {
                $("#spinner_gif_forgot").html('');
                $("#footer_submit").removeAttr("disabled");
                alertify.alert("Your enquiry has been submitted successfully");
                $("#footer_form")[0].reset();
            } else {
                $("#spinner_gif_forgot").html('');
                $("#footer_submit").removeAttr("disabled");
                if(Array.isArray(data.message) == false) 
                {
                    alertify.alert(data.message); 
                } else {
                    let errorMessage = "";
                    $(data.message).each(function(key, value) {
                        errorMessage = errorMessage + value;
                    });
                    alertify.alert(errorMessage);
                }
            }
        },
        error : function(error) {
            $("#spinner_gif_forgot").html('');
            $("#footer_submit").removeAttr("disabled");
            alertify.alert(eval(error));
        }
    });
});