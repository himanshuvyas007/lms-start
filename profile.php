<?php session_start(); ?>
<?php ob_start(); ?>
<?php include_once dirname(__FILE__) .'/lib/common-admin.php'; ?>
<?php redirect_to_login_front();?>
<?php include_once 'header_main.php'; ?>
<?php 
$userProfile = "select * from users where id = '".$_SESSION['activeUserIdFront']."' ";
$userProfileQuery = mysqli_query($dbConnection, $userProfile);
$userProfileQueryResult = mysqli_fetch_assoc($userProfileQuery);
?>
<div class="chatGrup">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="chatGrupLeft fadeInLeft wow animated">
                    <h3>View Profile</h3>
                    <div class="text-right">
                        <div class="row">
                            <div class="col-lg-2">
                                <a href="<?php echo BASE_URL.'edit_profile.php'; ?>" class="button">Edit Profile</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-5 col-lg-4">
                <div class="chatGrupLeft">
                <?php if(isset($userProfileQueryResult['image']) && $userProfileQueryResult['image'] != "" && file_exists(BASE_PATH.'/uploads/users/'.$userProfileQueryResult['image'])) { ?>
                    <img src="<?php echo BASE_URL.'/uploads/users/'.$userProfileQueryResult['image']; ?>">
                <?php } else { ?>
                    <img src="<?php echo BASE_URL.'/images/user_not_available.png'; ?>" width="100">
                <?php }?>
                </div>
            </div>
            <div class="col-12 col-md-7 col-lg-8">
                <div class="memberPage m30 fadeInRight wow animated">
                    <div class="myphto boxShe">
                        <h3><?php echo $userProfileQueryResult['first_name'].' '.$userProfileQueryResult['last_name']; ?></h3>
                        <div class="myphto1 myProfile">
                            <div class="table-responsive">
                                <table class="table">
                                    <tr>
                                        <td width="50%"><strong>Email Address:</strong> </td>
                                        <td><?php echo $userProfileQueryResult['email']; ?></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Phone:</strong> </td>
                                        <td><?php echo $userProfileQueryResult['phone']; ?></td>
                                    </tr>
                                    <tr>
                                        <td><strong>City:</strong> </td>
                                        <td><?php echo $userProfileQueryResult['city']; ?></td>
                                    </tr>
                                    <tr>
                                        <td><strong>State:</strong> </td>
                                        <td><?php echo $userProfileQueryResult['state']; ?></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Country:</strong> </td>
                                        <td><?php echo $userProfileQueryResult['country']; ?></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Zip/Postal Code:</strong> </td>
                                        <td><?php echo $userProfileQueryResult['zipcode']; ?></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Address:</strong> </td>
                                        <td><?php echo nl2br($userProfileQueryResult['address_1']); ?> <?php echo nl2br($userProfileQueryResult['address_2']); ?></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Profile created:</strong> </td>
                                        <td><?php echo date("M d, Y h:i A", strtotime($userProfileQueryResult['created'])); ?></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Profile updated:</strong> </td>
                                        <td><?php echo date("M d, Y h:i A", strtotime($userProfileQueryResult['modified'])); ?></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php include_once 'footer.php'; ?>
<?php ob_end_flush(); ?>