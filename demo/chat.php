 <!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">

    <title>P2P Text and Video Chat</title>

    <meta name="viewport" content="width=device-width,initial-scale=1">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <link rel="stylesheet" href="../css/demo.css" type="text/css">
</head>
<body>
    <div class="container-fluid">
        <div class="header fixed-top p-3">
            <div class="row align-items-center">
                <div class="col-md-4 d-none d-md-flex">
                    <input type="text" class="form-control rw-75 mx-auto search rounded-pill pl-4 pr-4"
                        placeholder="Search" id="searchConversation">
                </div>
                <div class="col-md">
                    <div class="row align-items-center" id="nameHeaderRow">
                        <div class="col-10 pr-0" id="chatRoomName">
                            <i class="fas fa-arrow-left fa-fw fa-lg fc-crimson d-inline-block d-md-none mr-3"
                                id="back-button"></i>
                            <img src="" alt="Profile Picture"
                                class="rounded-circle display-picture d-none d-md-inline-block mr-3 border"
                                id="currentChatRoomImage">
                            <h4 class="m-0 ft-poppins d-inline-block current-chat-name">
                                <span id="currentChatRoom"></span><span class="text-success online-badge"></span>
                            </h4>
                        </div>
                        <div class="col text-right">
                            <a href="#" id="video-call-button">
                                <!-- <i class="fas fa-video fa-fw fa-lg fc-crimson"></i> -->
                            </a>
                            <a href="#">
                                <input type="file" id="inputFile" hidden>
                                <i class="fas fa-paperclip fa-fw fa-lg fc-crimson" id="inputFileButton"></i>
                            </a>
                        </div>
                    </div>

                    <div class="row align-items-center" id="yourChatsHeading">
                        <div class="col-10">
                            <h4 class="m-0 ft-poppins d-inline-block current-chat-name">
                                Your Chats
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="content">
            <div class="row">
                <div class="col-md-4 sidebar shadow pl-0 pl-md-3">
                    <ul class="list-unstyled container" id="people-list"></ul>
                </div>
                <div class="col-md chat-column p-0">
                    <div class="chat-container pl-3 pr-3" id="chatScreen"><br><br></div>
                    <div class="input-div">
                        <div class="input-group">
                            <input id="inputMessage" type="text" class="form-control rounded-0"
                                placeholder="Type your message here…" autofocus>
                            <div class="input-group-prepend rounded-0 cursor-pointer">
                                <span class="input-group-text border-0 pl-4 pr-4" id="sendButton">
                                    <i class="fas fa-paper-plane"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="video-call-screen">
        <div id="calling">
            <img src="https://d33wubrfki0l68.cloudfront.net/02fc3819556fad224e910c2fe245e222bd8a84b2/378c2/images/avatar.jpg"
                alt="dp" class="rounded-circle img-thumbnail">
            <h2 class="ft-poppins m-0 text-center" id="calling-animation">Calling Dhruvik<span></span></h2>
        </div>

        <div id="on-call">
            <video oncontextmenu="return false;" id="video">
                <source src="https://www.w3schools.com/html/mov_bbb.mp4" type="video/mp4">
                Your browser does not support HTML5 video.
            </video>
        </div>

        <div id="video-call-actions">
            <center>
                <div class="mute d-inline-block mr-2">
                    <i class="fas fa-microphone-slash fa-lg"></i>
                </div>
                <div class="end-call shadow d-inline-block">
                    <i class="fas fa-phone-slash fa-lg"></i>
                </div>
                <div class="video-mute d-inline-block ml-2">
                    <i class="fas fa-video-slash fa-lg"></i>
                </div>
            </center>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="../js/app.js"></script>
    <script src="../js/chat.js"></script>

    <script>
        $(document).ready(() => {

            var init = () => {

                // GET USER ID FIRTS
                // Controller.globalVariables.currentUserID = 

                // WHEN PAGE LOADS LIST ALL CHAT CONVERSATION
                let chatRows = Controller.listAllChatrooms();
                $("#people-list").append(chatRows);

                // LOAD FIRST CHAT SCREEN
                let chatData = Controller.getChatRoomDetails(Controller.globalVariables.currentChatRoom);
                $("#currentChatRoom").text(chatData.chatRoomName);
                $("#chatScreen").html(chatData.finalRenderHtml);
                $("#currentChatRoomImage").attr('src', chatData.chatRoomImage);
            }

            // SEACRH IN THE LISTED CONVERSATION
            $("#searchConversation").keyup(function () {
                if ($(this).val() != '') {
                    let resultArray = Controller.seacrhConversation($(this).val());
                    let renderedSearchResult = Controller.listAllChatrooms(resultArray);
                    $("#people-list").html(renderedSearchResult);
                } else {
                    let chatRows = Controller.listAllChatrooms();
                    $("#people-list").html(chatRows);
                }

            });



            // call chat room details api on clicking contact

            $(".contactGrid").click(function () {
                alert("efewfewf");
            });

            // OPEN SELECT FILE FROM BUTTON
            $("#inputFileButton").click(function () {
                $("#inputFile").click();
            });

            // LISTEN FILE CHANGE EVENT 
            $("#inputFile").change(function () {
                let file = $(this)[0].files[0];
                Controller.uploadFile(file);
            });

            // LISTENING KEYUP EVENT WHILE TYPING
            $("#inputMessage").keyup((e) => {
                if (e.keyCode == 13) {
                    $("#sendButton").click();
                }
            });

            // LISTENING KEYUP EVENT WHILE TYPING
            $("#sendButton").on('click', () => {
                let message = $("#inputMessage").val();
                let renderedHtml = Controller.sendMessage(message);
                $("#chatScreen").append(renderedHtml);
                $("#inputMessage").val('');
            });

            init();

            API.listAllChatrooms().then(function(data){
                console.log(data);
            });
        });

        var setChatRoom = function (obj) {
            let chatRoomID = obj.getAttribute("chatroom-id");
            let chatData = Controller.getChatRoomDetails(chatRoomID);
            $("#currentChatRoom").text(chatData.chatRoomName);
            $("#chatScreen").html(chatData.finalRenderHtml);
            $("#currentChatRoomImage").attr('src', chatData.chatRoomImage);
        }
    </script>
</body>

</html>