<?php include_once dirname(__FILE__) .'/../header.php'; ?>
<?php $siteSettings = getSettings(); ?>
<?php include_once dirname(__FILE__) . '/../../lib/paginator.php'; ?>
<style type="text/css" href="<?php echo BASE_URL.'/css/pagination.css'; ?>"></style>
<div class="page-header">
    <h3 class="page-title">
        Content Pages
    </h3>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Content Pages</a></li>
            <li class="breadcrumb-item active" aria-current="page">Pages List</li>
        </ol>
    </nav>
</div>
<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <a href="<?php echo BASE_ADMIN_URL ?>/pages/add.php" class="btn btn-gradient-primary btn-fw">Add Page</a>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
       <?php include_once BASE_PATH.'/session_message.php'; ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>SL No</th>
                            <th width="50%">Page Title</th>
                            <th>Date</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $pages = new Paginator($siteSettings['rows_per_page'],'p');
                        $page_no = ((isset($_REQUEST['page']) && $_REQUEST['page'] != "") ? $_REQUEST['page'] : 1);
                        $offset = ($page_no - 1) * ROWS_PER_PAGE;
                        $users_lists = "select id, page_title, created from pages ";
                        $users_pagination_query = mysqli_query($dbConnection, $users_lists);
                        $users_pagination_count = mysqli_num_rows($users_pagination_query);
                        $pages->set_total($users_pagination_count);
                        $users_lists_limit = $users_lists. $pages->get_limit();
                        $users_lists_query = mysqli_query($dbConnection, $users_lists_limit);
                        $users_lists_num_rows = mysqli_num_rows($users_lists_query);
                        if($users_lists_num_rows > 0) {
                            $counter = 1;
                            while($users_lists_array = mysqli_fetch_array($users_lists_query)) { ?>
                            <tr>
                                <td><?php echo $counter + $offset; ?></td>
                                <td><?php echo $users_lists_array['page_title']; ?></td>
                                <td><?php echo date("M d, Y H:i A", strtotime($users_lists_array['created'])); ?></td>
                                <td>
                                    <a href="<?php echo BASE_ADMIN_URL.'/pages/edit.php?id='.$users_lists_array['id'].'&page='.$page_no ?>" title="Edit User"><i class="mdi mdi-pencil icon-sm"></i></a>
                                </td>
                            </tr>
                        <?php 
                            $counter = $counter + 1;
                        } 
                        ?>
                        <tr>
                            <td colspan="8" align="center"><?php echo $pages->page_links(); ?></td>
                        </tr>
                        <?php } else { ?>
                        <tr>
                            <td colspan="8" align="center">No pages exist</td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
<?php include_once dirname(__FILE__) .'/../footer.php'; ?>