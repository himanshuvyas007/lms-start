<div class="footer">
  <div class="footerT fw">
    <div class="container">
    <form action="" id="footer_form">
      <div class="row">
        <div class="col-md-3">
          <input name="footer_name" placeholder="Name" type="text" id='footer_name'>
        </div>
        <div class="col-md-3">
          <input name="footer_email" placeholder="Email" type="text" id='footer_email'>
        </div>
        <div class="col-md-4">
          <input name="footer_message" placeholder="Message" type="text" id='footer_message'>
        </div>
        <div class="col-md-2">
          <input name="SUBMIT" value="SUBMIT" type="button" id="footer_submit">
          <div id="spinner_gif_footer"></div>
        </div>
      </div>
      </form>
    </div>
  </div>
  <div class="footerM fw">
    <div class="container">
      <div class="row">
        <div class="col-md-4"> <img src="images/logo.png">
            <?php $pageContent = getPageContent(5); ?>
            <?php if($pageContent != "id_not_found" && $pageContent != "record_not_found") { ?>
              <?php echo $pageContent['content']; ?>
            <?php } ?>
        </div>
        <div class="col-md-2">
          <div class="fooul fw">
            <h4>Social</h4>
            <ul>
              <li><a href="https://facebook.com" target ='_blank'>Facebook</a></li>
              <li><a href="https://twitter.com" target ='_blank'>Twitter</a></li>
              <li><a href="https://instagram.com" target ='_blank'>Instagram</a></li>
            </ul>
          </div>
        </div>
        <div class="col-md-2">
          <div class="fooul fw">
            <h4>Support</h4>
            <ul>
              <li><a href="">Support Home</a></li>
              <li><a href="">Contact Us</a></li>
              <li><a href="">Uninstall</a></li>
            </ul>
          </div>
        </div>
        <div class="col-md-2">
          <div class="fooul fw">
            <h4>Legal</h4>
            <ul>
              <li><a href="<?php echo BASE_URL.'page.php?pageId=3' ?>">Terms of Service</a></li>
              <li><a href="<?php echo BASE_URL.'page.php?pageId=4' ?>">Privacy Policy</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="footerB fw">
    <p> Copyright © 2019 <a href="">Websibis</a> All rights reserved. </p>
  </div>
</div>
<script src="<?php echo BASE_URL.'js/jquery.min.js'?>"></script> 
<script src="<?php echo BASE_URL.'js/bootstrap.min.js'?>"></script> 
<script src="<?php echo BASE_URL.'js/slick.min.js'?>"></script> 
<script src="<?php echo BASE_URL.'js/wow.min.js'?>"></script> 
<script src="<?php echo BASE_URL.'js/custom.js'?>"></script>
<script src="<?php echo BASE_URL.'js/alertifyjs/alertify.min.js'?>"></script>
<script src="<?php echo BASE_URL.'js/login.js'?>"></script>
</body>
</html>