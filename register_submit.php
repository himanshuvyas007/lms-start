<?php
session_start();
include_once  dirname(__FILE__) .'/lib/common-admin.php';
use PHPMailer\PHPMailer\PHPMailer;
require dirname(__FILE__) .'/lib/vendor/autoload.php';
if(isset($_POST) && !empty($_POST))
{
    $validation_array = [];
    try {
        // validate all required fields

        if(!isset($_POST['first_name']) || $_POST['first_name'] == "")
            $validation_array['first_name'] = "Please enter your first name";

        if(!isset($_POST['last_name']) || $_POST['last_name'] == "")
            $validation_array['last_name'] = "Please enter your last name";

        if(!isset($_POST['email']) || $_POST['email'] == "")
            $validation_array['email'] = "Please enter your email address";

        if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
            $validation_array['email'] = "Please enter valid email address";

        if(!isset($_POST['password']) || $_POST['password'] == "")
            $validation_array['password'] = "Please enter password";

        if(strlen($_POST['password']) < 8)
            $validation_array['password'] = "Please enter password of minimum 8 characters long";

        if(!isset($_POST['confirm_password']) || $_POST['confirm_password'] == "")
            $validation_array['confirm_password'] = "Please confirm your password";

        if(isset($_POST['confirm_password']) && $_POST['confirm_password'] != $_POST['password'])
            $validation_array['confirm_password'] = "Passwords do not match";

        if(!isset($_POST['phone']) || $_POST['phone'] == "")
            $validation_array['phone'] = "Please enter your Mobile number";

        if(!is_numeric($_POST['phone']))
            $validation_array['phone'] = "Only numeric values are allowed";

        // check that email address already exists or not
        $check_duplicate_email = "select id from users where email = '".$_POST['email']."'";
        $check_duplicate_email_query = mysqli_query($dbConnection, $check_duplicate_email);
        $check_duplicate_email_query_array = mysqli_fetch_array($check_duplicate_email_query);
        if(isset($check_duplicate_email_query_array['id'])) 
        {
            $validation_array['email'] = "Email address already registered, please use different one";
        }
        if(!isset($validation_array) || empty($validation_array)) 
        {
            $save_user = "insert into users set first_name='".$_POST['first_name']."', last_name='".$_POST['last_name']."', email='".$_POST['email']."', phone='".$_POST['phone']."', password='".md5($_POST['password'])."', is_verified = 0, is_active = 1, created = '".date("Y-m-d H:i:s")."', modified='".date("Y-m-d H:i:s")."' ";
            $save_user_query = mysqli_query($dbConnection, $save_user);

            // send email to verify email address
            $verify_email_address_key = md5($check_duplicate_email_query_array['id']);
            $verify_email_link = "<a href='".BASE_URL."/verify_email.php?key=".$verify_email_address_key."&email=".$_POST['email']."'>".BASE_URL."verify_email.php?key=".$verify_email_address_key."&email=".$_POST['email']."</a>";
            
            // preparing email to send
            $mailBody = '<p>Please click below link to reset password</p>';
            $mailBody .= '<p>'.$verify_email_link.'</p>';
            $mailBody .= '<p>If you can\'t click on above link then copy and paste above url on browser and press enter.</p>';
            $mailBody .= '<p>Regards,<br/>'.SITE_TITLE.'</p>';
            $mail = new PHPMailer(true);
            $mail->addAddress($_POST['email'], $_POST['first_name']);
            $mail->setFrom(SITE_EMAIL);
            $mail->isHTML(true);
            $mail->Subject = "Verify your email address  - ".SITE_TITLE;
            $mail->msgHTML($mailBody);
            $mail->send();
            // update verify email address key
            $update_key = "update users set verify_email_address_key  = '".$verify_email_address_key."' where id = '".$check_duplicate_email_query_array['id']."'";
            $update_key_query = mysqli_query($dbConnection, $update_key);
            // mark as success
            echo json_encode(['status' => 200, 'message' => "Your account has been created successfully, please verify your email address from your inbox"]);
            die;
        } else {
            echo json_encode(['status' => 401, 'message' => $validation_array]);
            die;
        }
    } catch (Exception $e) {
        echo json_encode(['status' => 403, 'message' => "Something went wrong, please try again later"]);
        die;
    }
}