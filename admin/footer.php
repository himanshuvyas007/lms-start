<footer class="footer">
    <div class="d-sm-flex justify-content-center justify-content-sm-between">
        <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2017 <a href="javascript:void(0)" target="_blank"><?php echo SITE_TITLE; ?></a>. All rights reserved.</span>
    </div>
</footer>
<!-- partial -->
</div>
<!-- main-panel ends -->
</div>
<!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->

<!-- endinject -->
<!-- Plugin js for this page-->
<!-- End plugin js for this page-->
<!-- inject:js -->
<script src="<?php echo BASE_ADMIN_URL ?>/js/off-canvas.js"></script>
<script src="<?php echo BASE_ADMIN_URL ?>/js/misc.js"></script>
<!-- endinject -->
<!-- Custom js for this page-->
<script src="<?php echo BASE_ADMIN_URL ?>/js/dashboard.js"></script>
<!-- End custom js for this page-->
</body>

</html>