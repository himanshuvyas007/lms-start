$(document).ready(function() {
    $("button[type=submit]").on("click", function() {
        if($.trim($("#first_name").val()) == "")
        {
            $("#error_first_name").html("Please enter your first name");
            $("#first_name").focus();
            return false;
        } else {
            $("#error_first_name").html("");
        }
        if($.trim($("#last_name").val()) == "")
        {
            $("#error_last_name").html("Please enter your last name");
            $("#last_name").focus();
            return false;
        } else {
            $("#error_last_name").html("");
        }
        if($.trim($("#email").val()) == "")
        {
            $("#error_email").html("Please enter your email address");
            $("#email").focus();
            return false;
        } else {
            $("#error_email").html("");
        }

        if($.trim($("#email").val()) != "")
        {
            var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
            if (reg.test($("#email").val()) == false)
            {

                $("#error_email").html("Please enter valid email address");
                $("#email").focus();
                return false;
            } else {
                $("#error_email").html("");
            }
        }

        if($.trim($("#password").val()) == "") 
        {
            $("#error_password").html("Please enter password");
            $("#password").focus();
            return false;
        } else {
            $("#error_password").html("");
        }
        if($.trim($("#password").val()).length < 8) 
        {
            $("#error_password").html("Please enter password of minimum 8 characters long");
            $("#password").focus();
            return false;
        } else {
            $("#error_password").html("");
        }
        if($.trim($("#confirm_password").val()) == "") 
        {
            $("#error_confirm_password").html("Please confirm your password");
            $("#confirm_password").focus();
            return false;
        } else {
            $("#error_confirm_password").html("");
        }
        if($.trim($("#confirm_password").val()) != $.trim($("#password").val())) 
        {
            $("#error_confirm_password").html("Passwords do not match");
            $("#confirm_password").focus();
            return false;
        } else {
            $("#error_confirm_password").html("");
        }
        if($.trim($("#phone").val()) == "") 
        {
            $("#error_phone").html("Please enter your Mobile number");
            $("#phone").focus();
            return false;
        } else {
            $("#error_phone").html("");
        }
        if($.trim($("#phone").val()) != "")
        {
            if (isNaN($("#phone").val()))
            {
                $("#error_phone").html("Only numeric values are allowed");
                $("#phone").focus();
                return false;
            } else {
                $("#error_phone").html("");
            }
        }
        if($.trim($("#address1").val()) == "") 
        {
            $("#error_address1").html("Please enter your address");
            $("#address1").focus();
            return false;
        } else {
            $("#error_address1").html("");
        }
        if($.trim($("#country").val()) == "") 
        {
            $("#error_last_name").html("Please select your country");
            $("#country").focus();
            return false;
        } else {
            $("#error_country").html("");
        }
        if($.trim($("#state").val()) == "") 
        {
            $("#error_state").html("Please enter your state name");
            $("#state").focus();
            return false;
        } else {
            $("#error_state").html("");
        }
        if($.trim($("#city").val()) == "") 
        {
            $("#error_city").html("Please enter your city");
            $("#city").focus();
            return false;
        } else {
            $("#error_city").html("");
        }
        if($.trim($("#zipcode").val()) == "") 
        {
            $("#error_zipcode").html("Please enter your zip or postal code");
            $("#zipcode").focus();
            return false;
        } else {
            $("#error_zipcode").html("");
        }
    });
})