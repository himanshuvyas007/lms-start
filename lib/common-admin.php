<?php
include_once dirname(__FILE__) .'/connection.php';

/**
 * Function for redirecting to dashboard if session is active
 */
function redirect_to_dashboard()
{
    if(isset($_SESSION['active_user_id']) && $_SESSION['active_user_id'] != "")
    {
        redirect(BASE_ADMIN_URL.'/dashboard.php');
    }
}

/**
 * Function for redirecting to login if session is expired or not active
 */
function redirect_to_login()
{
    if(!isset($_SESSION['active_user_id']) || $_SESSION['active_user_id'] == "")
    {
        $_SESSION['error'] = "You have been logged out, please login again";
        redirect(BASE_ADMIN_URL);
    }
}

/**
 * Function to beautify array
 * 
 * @param array $array
 */
function dump($array)
{
     
    echo '<pre>';
    print_r($array);
    echo '</pre>';
}

/**
 * Function for redirection to respective URL
 * 
 * @param string $url
 */
function redirect($url)
{
    header("location: ".$url);
    exit();
}

/**
 * function to return retrieve site settings values
 *
 * @return array
 */
function getSettings()
{
    global $dbConnection;
    $sql = "select * from settings";
    $result = mysqli_query($dbConnection, $sql);
    return mysqli_fetch_assoc($result);
}

/**
 * function to retrieve content page data
 *
 * @param integer $pageId
 * 
 * @return string|array
 */
function getPageContent($pageId = "")
{
    if(!$pageId) {
        return "id_not_found";
    }
    global $dbConnection;
    $pageDetailsQuery = "select id, page_title, content from pages where id ='".$pageId."' ";
    $pageDetailsQueryResult = mysqli_query($dbConnection, $pageDetailsQuery);
    $pageContent = mysqli_fetch_assoc($pageDetailsQueryResult);
    if(empty($pageContent)) {
        return "record_not_found";
    } else {
        return $pageContent;
    }
}

/**
 * Function for redirecting to login if session is expired or not active in front panel
 */
function redirect_to_login_front()
{
    if(!isset($_SESSION['activeUserIdFront']) || $_SESSION['activeUserIdFront'] == "")
    {
        $_SESSION['error'] = "You have been logged out, please login again";
        redirect(BASE_URL);
    }
}