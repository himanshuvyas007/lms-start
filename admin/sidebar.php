<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item">
            <a class="nav-link" href="<?php echo BASE_ADMIN_URL ?>/dashboard.php">
                <span class="menu-title">Dashboard</span>
                <i class="mdi mdi-home menu-icon"></i>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
                <span class="menu-title">Users Management</span>
                <i class="menu-arrow"></i>
                <i class="mdi mdi-account menu-icon"></i>
            </a>
            <div class="collapse" id="ui-basic">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link" href="<?php echo BASE_ADMIN_URL ?>/users/lists.php">Users List</a></li>
                    <li class="nav-item"> <a class="nav-link" href="<?php echo BASE_ADMIN_URL ?>/users/add.php">Add User</a></li>
                </ul>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#pages" aria-expanded="false" aria-controls="ui-basic">
                <span class="menu-title">Content Pages</span>
                <i class="menu-arrow"></i>
                <i class="mdi mdi-google-pages menu-icon"></i>
            </a>
            <div class="collapse" id="pages">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link" href="<?php echo BASE_ADMIN_URL ?>/pages">Pages List</a></li>
                    <li class="nav-item"> <a class="nav-link" href="<?php echo BASE_ADMIN_URL ?>/pages/add.php">Add Page</a></li>
                </ul>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?php echo BASE_ADMIN_URL ?>/enquiries/index.php">
                <span class="menu-title">Enquiries</span>
                <i class="mdi mdi-settings menu-icon"></i>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?php echo BASE_ADMIN_URL ?>/settings/form.php">
                <span class="menu-title">Settings</span>
                <i class="mdi mdi-settings menu-icon"></i>
            </a>
        </li>
    </ul>
</nav>
