<?php session_start(); ?>
<?php ob_start(); ?>
<?php include_once dirname(__FILE__) .'/lib/common-admin.php'; ?>
<?php include_once 'header_main.php'; ?>
<div class="container">
    <?php 
    if(isset($_REQUEST['pageId']) && $_REQUEST['pageId'] != "") 
    {   
        $pageContent = getPageContent($_REQUEST['pageId']);
        if($pageContent == "id_not_found" || $pageContent == "record_not_found") {
            redirect(BASE_URL);
        }
    } else {
        redirect(BASE_URL);
    }
    ?>
    
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div class="chatGrupLeft fadeInLeft wow animated">
             <h3><?php echo $pageContent['page_title']; ?></h3>
            </div>
            <div class="content">
                <?php echo $pageContent['content']; ?>
            </div>
        </div>
    </div>
</div>
</div>
<?php include_once 'footer.php'; ?>
<?php ob_end_flush(); ?>