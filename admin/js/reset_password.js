$(document).ready(function() {
    $("button[type=submit]").on("click", function() {
        if($.trim($("#password").val()) == "") 
        {
            $("#error_password").html("Please enter your new password");
            $("#password").focus();
            return false;
        } else {
            $("#error_password").html("");
        }
        if($.trim($("#confirm_password").val()) == "") 
        {
            $("#error_confirm_password").html("Please confirm your password");
            $("#confirm_password").focus();
            return false;
        } else {
            $("#error_confirm_password").html("");
        }
        if($.trim($("#confirm_password").val()) != $.trim($("#password").val())) 
        {
            $("#error_confirm_password").html("Passwords do not match");
            $("#confirm_password").focus();
            return false;
        } else {
            $("#error_confirm_password").html("");
        }
    });
})