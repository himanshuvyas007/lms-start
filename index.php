<?php session_start(); ?>
<?php ob_start(); ?>
<?php include_once dirname(__FILE__) .'/lib/common-admin.php'; ?>
<?php include_once 'header.php'; ?>
<div class="maindiv">
  <div class="new_poeple">
    <div class="container"> <img src="images/new_people.png">
      <h3 class="slideInUp wow">Discover the world of websibis - <span>Meet New People</span></h3>
    </div>
  </div>
  <div class="say_about">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h4 class="textHed tC slideInDown wow">What People Say About Us</h4>
          <div class="slider testiSlide slideInUp wow">
            <div class="say_about1"> <img src="images/say1.jpg" class="sayImg">
              <p>Love how we can interact with people and get to know people virtually </p>
              <h5>Robert K.</h5>
              <span>New York</span>
              <div class="saYrating fw m10"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star notR"></i> <i class="fa fa-star notR"></i> </div>
            </div>
            <div class="say_about1"> <img src="images/say2.jpg" class="sayImg">
              <p>Love how we can interact with people and get to know people virtually </p>
              <h5>Robert K.</h5>
              <span>New York</span>
              <div class="saYrating fw m10"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star notR"></i> </div>
            </div>
            <div class="say_about1"> <img src="images/say3.jpg" class="sayImg">
              <p>Love how we can interact with people and get to know people virtually </p>
              <h5>Robert K.</h5>
              <span>New York</span>
              <div class="saYrating fw m10"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star notR"></i> <i class="fa fa-star notR"></i> <i class="fa fa-star notR"></i> </div>
            </div>
            <div class="say_about1"> <img src="images/say1.jpg" class="sayImg">
              <p>Love how we can interact with people and get to know people virtually </p>
              <h5>Robert K.</h5>
              <span>New York</span>
              <div class="saYrating fw m10"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star notR"></i> <i class="fa fa-star notR"></i> </div>
            </div>
            <div class="say_about1"> <img src="images/say2.jpg" class="sayImg">
              <p>Love how we can interact with people and get to know people virtually </p>
              <h5>Robert K.</h5>
              <span>New York</span>
              <div class="saYrating fw m10"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star notR"></i> </div>
            </div>
            <div class="say_about1"> <img src="images/say3.jpg" class="sayImg">
              <p>Love how we can interact with people and get to know people virtually </p>
              <h5>Robert K.</h5>
              <span>New York</span>
              <div class="saYrating fw m10"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star notR"></i> <i class="fa fa-star notR"></i> <i class="fa fa-star notR"></i> </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="chatRoom">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h3 class="textHed slideInUp wow">Group Chat Rooms</h3>
          <p class="textP1 slideInUp wow">Browse through thousands of chat rooms and choose from hundreds of topics.</p>
          <a href="" class="btnFixed1 zoomIn wow"> Get Websibis Free </a> <img class="slideInUp wow" src="images/chatRoom.png"> </div>
      </div>
    </div>
  </div>
  <div class="videoChat">
    <div class="container">
      <div class="row">
        <div class="col-lg-5 col-md-6">
          <h3 class="textHed slideInUp wow">Video chat with friends</h3>
          <p class="textP1 slideInUp wow">Start a video chat with friends, new and old. </p>
          <a href="" class="btnFixed1 zoomIn wow"> Get Websibis Free </a> </div>
        <div class="col-lg-7 col-md-6">
          <div class="row">
            <div class="col-md-4">
              <div class="vidoeChat fadeInRight wow"> <img src="images/videoC01.png"> </div>
            </div>
            <div class="col-md-4">
              <div class="vidoeChat fadeInRight wow" style="animation-delay:0.3s;"> <img src="images/videoC02.png"> </div>
            </div>
            <div class="col-md-4">
              <div class="vidoeChat fadeInRight wow" style="animation-delay:0.6s;"> <img src="images/videoC03.png"> </div>
            </div>
            <div class="col-md-4">
              <div class="vidoeChat fadeInRight wow" style="animation-delay:0.9s;"> <img src="images/videoC01.png"> </div>
            </div>
            <div class="col-md-4">
              <div class="vidoeChat fadeInRight wow" style="animation-delay:1.2s;"> <img src="images/videoC02.png"> </div>
            </div>
            <div class="col-md-4">
              <div class="vidoeChat fadeInRight wow" style="animation-delay:1.5s;"> <img src="images/videoC03.png"> </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="giftSti">
    <div class="container">
      <div class="row">
        <div class="col-md-7 fadeInLeft wow">
          <h4 class="textHed">Gifts and Stickers</h4>
          <p class="textP1">Send a gift to break the ice with a new pal or an entire chatroom. Use our fun collection of stickers to communicate with friends.</p>
          <a href="" class="btnFixed1"> Get Websibis Free </a> </div>
        <div class="col-md-5 fadeInRight wow"> <img src="images/giftSti.png"> </div>
      </div>
    </div>
  </div>
</div>

<?php include_once 'footer.php'; ?>
<?php ob_end_flush(); ?>