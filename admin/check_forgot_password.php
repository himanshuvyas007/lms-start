<?php
session_start();
include_once '../lib/common-admin.php';
use PHPMailer\PHPMailer\PHPMailer;
require '../lib/vendor/autoload.php';
if(isset($_POST) && !empty($_POST))
{
    // fetch user details by supplied email and password
    $check_user_for_login = "select id, first_name, last_name, email, is_active from users where email = '".mysqli_real_escape_string($dbConnection, $_POST['email'])."'";
    $check_user_for_login_query = mysqli_query($dbConnection, $check_user_for_login);
    $check_user_for_login_result = mysqli_fetch_array($check_user_for_login_query, MYSQLI_ASSOC);
    if(!empty($check_user_for_login_result)) 
    {
        if($check_user_for_login_result['is_active'] == true) 
        {
            $forgot_password_key = md5($check_user_for_login_result['id']);
            $reset_password_link = "<a href='".BASE_ADMIN_URL."/reset_password.php?key=".$forgot_password_key."&email=".$check_user_for_login_result['email']."'>".BASE_ADMIN_URL."/reset_password.php?key=".$forgot_password_key."&email=".$check_user_for_login_result['email']."</a>";
            
            // preparing email to send
            $mailBody = '<p>Please click below link to reset password</p>';
            $mailBody .= '<p>'.$reset_password_link.'</p>';
            $mailBody .= '<p>If you can\'t click on above link then copy and paste above url on browser and press enter.</p>';
            $mailBody .= '<p>Regards,<br/>'.SITE_TITLE.'</p>';
            $mail = new PHPMailer(true);
            $mail->addAddress($check_user_for_login_result['email'], $check_user_for_login_result['first_name']);
            $mail->setFrom(SITE_EMAIL);
            $mail->isHTML(true);
            $mail->Subject = "Reset your password  - ".SITE_TITLE;
            $mail->msgHTML($mailBody);
            
            try {
                $mail->send();
                // update forgot password key
                $update_key = "update users set forgot_password_key  = '".$forgot_password_key."' where id = '".$check_user_for_login_result['id']."'";
                $update_key_query = mysqli_query($dbConnection, $update_key);
                // redirect back to forgot password with success message
                $_SESSION['success'] = "An email has been sent to your registered email address, please check your inbox and reset your password";
            } catch (\Exception $ex) {
                $_SESSION['error'] = $ex->getMessage();
            }
            redirect(BASE_ADMIN_URL.'/forgot_password.php');
        } else {
            // redirect to login page
            $_SESSION['error'] = "Your account has been deactivated by administrator";
            redirect(BASE_ADMIN_URL.'/forgot_password.php');
        }
    } else {
        // redirect to login page
        $_SESSION['error'] = "Email address does not exist";
        redirect(BASE_ADMIN_URL.'/forgot_password.php');
    }
}