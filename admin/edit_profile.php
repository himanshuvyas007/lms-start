<?php include_once dirname(__FILE__) . '/header.php'; ?>
<script type="text/javascript" src="<?php echo BASE_ADMIN_URL.'/js/file-upload.js'; ?>"></script>
<script type="text/javascript" src="<?php echo BASE_ADMIN_URL.'/js/users/edit_user.js'; ?>"></script>
<div class="page-header">
    <h3 class="page-title">
      Edit Profile
    </h3>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Edit Profile</li>
        </ol>
    </nav>
</div>
<?php if(isset($validation_array['global']) && !empty($validation_array['global'])) { ?>
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card text-center">
            <div class="alert alert-danger"><?php echo $validation_array['global']; ?></div>
        </div>
    </div>
<?php } ?>
<div class="row">
    <?php 
    // fetch user details by id
    $users_details = "select id, first_name, last_name, email, phone, country, address_1, address_2, state, city, zipcode, image from users where role_id ='".ADMINISTRATOR_ID."'";
    $users_details_query = mysqli_query($dbConnection, $users_details);
    $users_details_array = mysqli_fetch_array($users_details_query);
    if(isset($_POST) && !empty($_POST))
    {
        // save data
        $validation_array = [];
        try {
            
            // check image is uploaded or not

            $user_image = "";            
            if(isset($_FILES['image']) && !empty($_FILES['image']) && $_FILES['image']['name'] != "")
            {
                // check file upload error
                $file_upload_error = $_FILES['image']['error'];

                switch ($file_upload_error)
                {
                    case "1":
                        $validation_array['image'] = "Uploaded image size is too big, please upload small image";
                        break;
                    case "2":
                        $validation_array['image'] = "Uploaded image file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form";
                        break;
                    case "3":
                        $validation_array['image'] = "Image file was only partially uploaded.";
                        break;
                    case "4":
                        $validation_array['image'] = "File was not uploaded, please try again";
                        break;
                }
                // check valid extensions of image
                $image_extension = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);
                if(in_array($image_extension, ALLOWED_IMAGE_EXTENSIONS))
                {
                    $user_image = md5(time()).'.'.$image_extension;
                    move_uploaded_file($_FILES['image']['tmp_name'], BASE_PATH.'/uploads/users/'.$user_image);
                } else {
                    $validation_array['image'] = "Uploaded image should be of ".implode(", ", ALLOWED_IMAGE_EXTENSIONS)." type only ";
                }
            }

            // validate all required fields

            if(!isset($_POST['first_name']) || $_POST['first_name'] == "")
                $validation_array['first_name'] = "Please enter your first name";

            if(!isset($_POST['last_name']) || $_POST['last_name'] == "")
                $validation_array['last_name'] = "Please enter your last name";

            if(!isset($_POST['email']) || $_POST['email'] == "")
                $validation_array['email'] = "Please enter your email address";

            if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
                $validation_array['email'] = "Please enter valid email address";

            if(!isset($_POST['phone']) || $_POST['phone'] == "")
                $validation_array['phone'] = "Please enter your Mobile number";

            if(!is_numeric($_POST['phone']))
                $validation_array['phone'] = "Only numeric values are allowed";
            
            if(!isset($_POST['address_1']) || $_POST['address_1'] == "")
                $validation_array['address_1'] = "Please enter your address";
            
            if(!isset($_POST['state']) || $_POST['state'] == "")
                $validation_array['country'] = "Please select your state";

            if(!isset($_POST['city']) || $_POST['city'] == "")
                $validation_array['city'] = "Please select your city";
                
            if(!isset($_POST['zipcode']) || $_POST['zipcode'] == "")
                $validation_array['zipcode'] = "Please enter your zip or postal code";

            if(!isset($validation_array) || empty($validation_array)) 
            {
                $save_user = "update users set first_name='".$_POST['first_name']."', last_name='".$_POST['last_name']."', email='".$_POST['email']."', phone='".$_POST['phone']."', country='".$_POST['country']."', 
                              address_1='".$_POST['address_1']."', address_2='".$_POST['address_2']."', state='".$_POST['state']."', 
                              city='".$_POST['city']."', zipcode='".$_POST['zipcode']."'";

                if($user_image != "") 
                {
                    $save_user .= ", image='".$user_image."' ";
                }

                $save_user .= ", modified='".date("Y-m-d H:i:s")."' where role_id ='".ADMINISTRATOR_ID."' ";

                $save_user_query = mysqli_query($dbConnection, $save_user);

                $_SESSION['success'] = "Profile has been saved successfully";
                redirect(BASE_ADMIN_URL.'/edit_profile.php');
            }
            
        } catch (\Exception $e) {
            $validation_array['global'] = $e->getMessage();
        }
    }
    ?>
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <form class="forms-sample" action="" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="first_name">First Name</label>
                        <input type="text" class="form-control" id="first_name" maxlength="100" name="first_name" value="<?php echo ((isset($_POST['first_name']) && $_POST['first_name'] != "") ? $_POST['first_name'] : $users_details_array['first_name']); ?>">
                        <div class="error_message" id="error_first_name"><?php echo ((isset($validation_array['first_name']) && !empty($validation_array['first_name'])) ? $validation_array['first_name'] : "");?></div>
                    </div>
                    <div class="form-group">
                        <label for="last_name">Last Name</label>
                        <input type="text" class="form-control" id="last_name" maxlength="100" name="last_name" value="<?php echo ((isset($_POST['last_name']) && $_POST['last_name'] != "") ? $_POST['last_name'] : $users_details_array['last_name']); ?>">
                        <div class="error_message" id="error_last_name"><?php echo ((isset($validation_array['last_name']) && !empty($validation_array['last_name'])) ? $validation_array['last_name'] : "");?></div>
                    </div>
                    <div class="form-group">
                        <label for="email">Email address</label>
                        <input type="email" class="form-control" id="email" name="email" readonly="readonly" value="<?php echo ((isset($_POST['email']) && $_POST['email'] != "") ? $_POST['email'] : $users_details_array['email']); ?>">
                        <div class="error_message" id="error_email"><?php echo ((isset($validation_array['email']) && !empty($validation_array['email'])) ? $validation_array['email'] : "");?></div>
                    </div>
                    <div class="form-group">
                        <label for="phone">Mobile Number</label>
                        <input type="text" class="form-control" id="phone" name="phone" value="<?php echo ((isset($_POST['phone']) && $_POST['phone'] != "") ? $_POST['phone'] : $users_details_array['phone']); ?>">
                        <div class="error_message" id="error_phone"><?php echo ((isset($validation_array['phone']) && !empty($validation_array['phone'])) ? $validation_array['phone'] : "");?></div>
                    </div>
                    <div class="form-group">
                        <label for="address1">Address 1</label>
                        <input type="text" class="form-control" id="address1" name="address_1" value="<?php echo ((isset($_POST['address_1']) && $_POST['address_1'] != "") ? $_POST['address_1'] : $users_details_array['address_1']); ?>">
                        <div class="error_message" id="error_address1"><?php echo ((isset($validation_array['address_1']) && !empty($validation_array['address_1'])) ? $validation_array['address_1'] : "");?></div>
                    </div>
                    <div class="form-group">
                        <label for="address_2">Address 2</label>
                        <input type="text" class="form-control" id="address_2" name="address_2" value="<?php echo ((isset($_POST['address_2']) && $_POST['address_2'] != "") ? $_POST['address_2'] : $users_details_array['address_2']); ?>">
                    </div>
                    <div class="form-group">
                        <label for="phone">Country</label>
                        <select name="country" id="country" class="form-control">
                            <option value="">--Select--</option>
                            <?php 
                            $country_list = file_get_contents(BASE_PATH.'/countries.json');
                            $country_array = json_decode($country_list);
                            foreach ($country_array as $key => $countryIndex) 
                            {
                                echo '<option value="'.$countryIndex->name.'" '.((isset($_POST['country']) && $_POST['country'] == $countryIndex->name) ?  "selected='selected'" : ((isset($users_details_array['country']) && $users_details_array['country'] == $countryIndex->name) ? "selected='selected'" : "")).'>'.$countryIndex->name.'</option>'.'\n\t';
                            }
                            ?>
                        </select>
                        <div class="error_message" id="error_country"><?php echo ((isset($validation_array['country']) && !empty($validation_array['country'])) ? $validation_array['country'] : "");?></div>
                    </div>
                    <div class="form-group">
                        <label for="state">State</label>
                        <input type="text" class="form-control" id="state" name="state" value="<?php echo ((isset($_POST['state']) && $_POST['state'] != "") ? $_POST['state'] : $users_details_array['state']); ?>">
                        <div class="error_message" id="error_state"><?php echo ((isset($validation_array['state']) && !empty($validation_array['state'])) ? $validation_array['state'] : "");?></div>
                    </div>
                    <div class="form-group">
                        <label for="city">City</label>
                        <input type="text" class="form-control" id="city" name="city" value="<?php echo ((isset($_POST['city']) && $_POST['city'] != "") ? $_POST['city'] : $users_details_array['city']); ?>">
                        <div class="error_message" id="error_city"><?php echo ((isset($validation_array['city']) && !empty($validation_array['city'])) ? $validation_array['city'] : "");?></div>
                    </div>
                    <div class="form-group">
                        <label for="zipcode">Zip/Postal Code</label>
                        <input type="text" class="form-control" id="zipcode" name="zipcode" value="<?php echo ((isset($_POST['zipcode']) && $_POST['zipcode'] != "") ? $_POST['zipcode'] : $users_details_array['zipcode']); ?>">
                        <div class="error_message" id="error_zipcode"><?php echo ((isset($validation_array['zipcode']) && !empty($validation_array['zipcode'])) ? $validation_array['zipcode'] : "");?></div>
                    </div>
                    <div class="form-group">
                        <label>Avatar</label>
                        <input type="file" name="image" class="file-upload-default">
                        <div class="input-group col-xs-12">
                            <input type="text" class="form-control file-upload-info" disabled placeholder="Upload Image">
                            <span class="input-group-append">
                                <button class="file-upload-browse btn btn-gradient-primary" type="button">Upload</button>
                            </span>
                        </div><br/>
                        <?php if(isset($users_details_array['image']) && $users_details_array['image'] != "" && file_exists(BASE_PATH.'/uploads/users/'.$users_details_array['image'])) { ?>
                            <img src="<?php echo BASE_URL.'/uploads/users/'.$users_details_array['image']; ?>" width="100">
                        <?php } else { ?>
                            <img src="<?php echo BASE_ADMIN_URL.'/img/no-available-image.png'; ?>" width="100">
                        <?php }?>
                        <div class="error_message">
                            <?php echo ((isset($validation_array['image']) && !empty($validation_array['image'])) ? $validation_array['image'] : "");?>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-gradient-primary mr-2">Save Profile</button>
                    <a href="<?php echo BASE_ADMIN_URL.'/users/lists.php'; ?>" class="btn btn-light">Cancel</a>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
<?php include_once dirname(__FILE__) . '/footer.php'; ?>