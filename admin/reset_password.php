<?php session_start(); ?>
<?php ob_start(); ?>
<?php include_once '../lib/common-admin.php'; ?>
<?php redirect_to_dashboard(); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Reset Password - <?php echo SITE_TITLE; ?></title>
        <!-- plugins:css -->
        <link rel="stylesheet" href="<?php echo BASE_ADMIN_URL ?>/css/materialdesignicons.min.css">
        <link rel="stylesheet" href="<?php echo BASE_ADMIN_URL ?>/css/vendor.bundle.base.css">
        <!-- endinject -->
        <!-- plugin css for this page -->
        <!-- End plugin css for this page -->
        <!-- inject:css -->
        <link rel="stylesheet" href="<?php echo BASE_ADMIN_URL ?>/css/style.css">
        <link rel="stylesheet" href="<?php echo BASE_ADMIN_URL ?>/css/custom.css">
        <!-- endinject -->
        <link rel="shortcut icon" href="<?php echo BASE_ADMIN_URL ?>/img/favicon.png" />
    </head>
    <body>
        <?php 
        if(isset($_REQUEST['key']) && $_REQUEST['key'] != "" && isset($_REQUEST['email']) && $_REQUEST['email'] != "")
        {
            // check email address exist or not
            
            $check_email_exist = "select id, is_active from users where email = '".$_REQUEST['email']."'";
            $check_email_exist_query = mysqli_query($dbConnection, $check_email_exist);
            $check_email_exist_array = mysqli_fetch_array($check_email_exist_query);
            if(!empty($check_email_exist_array))
            {
                if($check_email_exist_array['is_active'] == true)
                {
                    // check valid key email pair
                    $check_valid_key = "select id, email from users where forgot_password_key = '".$_REQUEST['key']."' and email = '".$_REQUEST['email']."' ";
                    $check_valid_key_query = mysqli_query($dbConnection, $check_valid_key);
                    $check_valid_key_array = mysqli_fetch_array($check_valid_key_query);
                    if(!empty($check_valid_key_array))
                    {
                        if(isset($_POST) && !empty($_POST)) {
                            $update_password = "update users set password ='". md5($_POST['password'])."', forgot_password_key = null where email = '".$_REQUEST['email']."'";
                            $update_password_query = mysqli_query($dbConnection, $update_password);
                            $_SESSION['success'] = "Your password has been reset successfully, please login to continue";
                            redirect(BASE_ADMIN_URL);
                        }
                    } else {
                        $_SESSION['error'] = "Either key is not valid or not associated with supplied email";
                        redirect(BASE_ADMIN_URL.'/forgot_password.php');
                    }
                } else {
                    $_SESSION['error'] = "Your email account has been deactivated by administrator.";
                    redirect(BASE_ADMIN_URL.'/forgot_password.php');
                }
            } else {
                $_SESSION['error'] = "Email address does not exist";
                redirect(BASE_ADMIN_URL.'/forgot_password.php');
            }
        } else {
            $_SESSION['error'] = "Invalid request, please try again";
            redirect(BASE_ADMIN_URL.'/forgot_password.php');
        }
        ?>
        <div class="container-scroller">
            <div class="container-fluid page-body-wrapper full-page-wrapper">
                <div class="content-wrapper d-flex align-items-center auth">
                    <div class="row w-100">
                        <div class="col-lg-4 mx-auto">
                            <div class="auth-form-light text-left p-5">
                                <?php include_once '../session_message.php'; ?>
                                <h4>Reset Password</h4>
                                <form class="pt-3" action="" method="post">
                                    <div class="form-group">
                                        <input name="password" type="password" class="form-control form-control-lg" id="password" placeholder="Password" value="">
                                        <div class="error_message" id="error_password"></div>
                                    </div>
                                    <div class="form-group">
                                        <input name="confirm_password" type="password" class="form-control form-control-lg" id="confirm_password" placeholder="Password" value="">
                                        <div class="error_message" id="error_confirm_password"></div>
                                    </div>
                                    <div class="mt-3">
                                        <button class="btn btn-block btn-gradient-primary btn-lg font-weight-medium auth-form-btn" type="submit">Reset Password</button>
                                    </div>
                                    <div class="my-2 d-flex justify-content-between align-items-center">
                                        <a href="<?php echo BASE_ADMIN_URL ?>" class="auth-link text-black">Back to Login</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- content-wrapper ends -->
            </div>
            <!-- page-body-wrapper ends -->
        </div>
        <!-- container-scroller -->
        <!-- plugins:js -->
        <script src="<?php echo BASE_ADMIN_URL ?>/js/vendor.bundle.base.js"></script>
        <script src="<?php echo BASE_ADMIN_URL ?>/js/vendor.bundle.addons.js"></script>
        <!-- endinject -->
        <!-- inject:js -->
        <script src="<?php echo BASE_ADMIN_URL ?>/js/off-canvas.js"></script>
        <script src="<?php echo BASE_ADMIN_URL ?>/js/misc.js"></script>
        <script src="<?php echo BASE_ADMIN_URL ?>/js/reset_password.js"></script>
        <!-- endinject -->
    </body>

</html>
<?php ob_end_flush(); ?>