<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->

    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="css/styles.css" rel="stylesheet" type="text/css">
    <link href="css/custom.css" rel="stylesheet" type="text/css">
    <link href="css/slick.css" rel="stylesheet" type="text/css">
    <link href="css/responsive.css" rel="stylesheet" type="text/css">
    <link href="css/animate.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?php echo BASE_URL.'js/alertifyjs/css/alertify.min.css'?>">
    <script type="text/javascript"> var BASE_URL = '<?php echo BASE_URL; ?>' </script>
    <title>Websibis</title>
</head>

<body>
    <?php include_once 'header_menus.php'; ?>
    <div class="homeSlide">
        <div class="uc-shape-devider uc-shape-devider-bottom"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="slidemid slideInUp wow">
                        <h2>Join the Conversation</h2>
                        <p>Start a free video chat, voice, or text group chat </p>
                        <a href="" class="btnFixed zoomIn wow"> Get Websibis Free </a>
                        <div class="couterdiv slideInDown wow">
                            <p>Members Online Now</p>
                            <ul>
                                <li>1</li>
                                <li>2</li>
                                <li>2</li>
                                <li>4</li>
                                <li>7</li>
                                <li>9</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>