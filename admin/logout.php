<?php
session_start();
include_once '../lib/common-admin.php';
session_destroy();
$_SESSION['success'] = "You have been logged out successfully";
redirect(BASE_ADMIN_URL);