<?php
session_start();
include_once '../lib/common-admin.php';
if(isset($_POST) && !empty($_POST))
{
    // fetch user details by supplied email and password
    $check_user_for_login = "select id, email, is_active from users where email = '".mysqli_real_escape_string($dbConnection, $_POST['email'])."' and password = '".md5($_POST['password'])."'";
    $check_user_for_login_query = mysqli_query($dbConnection, $check_user_for_login);
    $check_user_for_login_result = mysqli_fetch_array($check_user_for_login_query, MYSQLI_ASSOC);
    if(!empty($check_user_for_login_result)) 
    {
        if($check_user_for_login_result['is_active'] == true) 
        {
            // check is remember is checked
            if(isset($_POST['remember_me']) && $_POST['remember_me'] == '1') 
            {
                $_COOKIE['ccokie_login_email'] = $_POST['email'];
                $_COOKIE['cookie_login_password'] = $_POST['password'];
                $_COOKIE['cookie_login_remember_me'] = $_POST['remember_me'];
            } else {
                $_COOKIE['ccokie_login_email'] = "";
                $_COOKIE['cookie_login_password'] = "";
                $_COOKIE['cookie_login_remember_me'] = "";
            }
            $_SESSION['active_user_id'] = $check_user_for_login_result['id'];
            // update last login of admin
            $update_last_login = "update users set last_login  = '".date("Y-m-d H:i:s")."' where id = '".$check_user_for_login_result['id']."'";
            $update_last_login_query = mysqli_query($dbConnection, $update_last_login);
            
            // redirect to dashboard
            redirect(BASE_ADMIN_URL.'/dashboard.php');
        } else {
            // redirect to login page
            $_SESSION['error'] = "Your account has been deactivated by administrator";
            redirect(BASE_ADMIN_URL);
        }
    } else {
        // redirect to login page
        $_SESSION['error'] = "Invalid email address or password entered";
        redirect(BASE_ADMIN_URL);
    }
}