<?php session_start(); ?>
<?php ob_start(); ?>
<?php include_once dirname(__FILE__) .'/lib/common-admin.php'; ?>
<?php redirect_to_login_front();?>
<?php include_once 'header_main.php'; ?>
<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <?php
            if(isset($_POST) && !empty($_POST)) {
                //check old password is correct or not
                $check_old_password = "select id from users where password = '" . md5($_POST['old_password']) . "' and id = '" . $_SESSION['activeUserIdFront'] . "' ";
                $check_old_password_query = mysqli_query($dbConnection, $check_old_password);
                $check_old_password_array = mysqli_fetch_array($check_old_password_query);
                if(!empty($check_old_password_array)) {
                    $update_password = "update users set password ='" . md5($_POST['new_password']) . "' where id = '" . $_SESSION['activeUserIdFront'] . "'";
                    $update_password_query = mysqli_query($dbConnection, $update_password);
                    $_SESSION['success'] = "Your password has been changed successfully";
                    redirect(BASE_URL . 'change_password.php');
                }
                else {
                    $_SESSION['error'] = "You have entered wrong old password";
                    redirect(BASE_URL . 'change_password.php');
                }
            }
            ?>
            <?php include_once 'session_message.php' ;?>
            <div class="chatGrupLeft fadeInLeft wow animated">
                <h3>Change Password</h3>
            </div>
            <div class="content">
                <form class="forms-sample" method="post" action="<?php echo BASE_URL . '/change_password.php' ?>">
                    <div class="row">
                        <div class="col-sm-6 col-md-6">
                            <div class="form-group">
                                <label for="old_password">Old Password</label>
                                <input type="password" class="form-control" id="old_password" name="old_password">
                                <div class="error_message" id="error_old_password"></div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6">
                            <div class="form-group">
                                <label for="new_password">New Password</label>
                                <input type="password" class="form-control" id="new_password" name="new_password">
                                <div class="error_message" id="error_new_password"></div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-sm-6 col-md-6">
                            <div class="form-group">
                                <label for="confirm_change_password">Confirm Password</label>
                                <input type="password" class="form-control" id="confirm_change_password" name="confirm_change_password">
                                <div class="error_message" id="error_confirm_change_password"></div>
                            </div>
                        </div>
                    </div>
                    <div class="mb-10"></div>
                    <div class="row">
                        <div class="col-sm-2 col-md-2">
                            <div class="form-group">
                                <input type="submit" class="btn btn-gradient-primary mr-2" value="Submit">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
<?php include_once 'footer.php'; ?>
<script src="<?php echo BASE_URL.'js/change_password.js' ?>"></script>
<?php ob_end_flush(); ?>