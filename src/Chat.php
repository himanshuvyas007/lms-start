<?php
namespace MyApp;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

class Chat implements MessageComponentInterface {
    protected $clients;
    private $rooms;

    public function __construct() {        
        $this->clients = new \SplObjectStorage;
        $this->rooms = [];
        echo "SOCKET SERVER STARTED!";
    }

    public function onOpen(ConnectionInterface $conn) {
        // Store the new connection to send messages to later
        $this->clients->attach($conn);

        echo "New connection! ({$conn->resourceId})\n";
    }

    public function onMessage(ConnectionInterface $from, $msg) {        
        $data = json_decode($msg);
        $action = $data->action;
        $room = isset($data->room) ? $data->room : "";        
        
        if(($action == 'subscribe') && $room){            
            if((array_key_exists($room, $this->rooms) && !in_array($from, $this->rooms[$room])) || !array_key_exists($room, $this->rooms)){                
                if(isset($this->rooms[$room]) && count($this->rooms[$room]) >= 4){                    
                    $msg_to_send = json_encode(['action'=>'subscribeRejected']);                
                    $from->send($msg_to_send);
                }
                
                else{
                    $this->rooms[$room][] = $from;//subscribe user to room                
                    $this->notifyUsersOfConnection($room, $from);
                }
            }
            
            else{
                //tell user he has subscribed on another device/browser
                $msg_to_send = json_encode(['action'=>'subscribeRejected']);
                
                $from->send($msg_to_send);
            }
        }
        
        //for other actions
        else if($room && isset($this->rooms[$room])){
            //send to everybody subscribed to the room received except the sender
            foreach($this->rooms[$room] as $client){
                if ($client !== $from) {
                    $client->send($msg);
                }
            }
        }
    }

    public function onClose(ConnectionInterface $conn) {
        // The connection is closed, remove it, as we can no longer send it messages
        $this->clients->detach($conn);

        echo "Connection {$conn->resourceId} has disconnected\n";
        if(count($this->rooms)){//if there is at least one room created
            foreach($this->rooms as $room=>$arr_of_subscribers){//loop through the rooms
                foreach ($arr_of_subscribers as $key=>$ratchet_conn){//loop through the users connected to each room
                    if($ratchet_conn == $conn){//if the disconnecting user subscribed to this room
                        unset($this->rooms[$room][$key]);//remove him from the room
                        
                        //notify other subscribers that he has disconnected
                        $this->notifyUsersOfDisconnection($room, $conn);
                    }
                }
            }
        }
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        echo "An error has occurred: {$e->getMessage()}\n";

        $conn->close();
    }

    private function notifyUsersOfConnection($room, $from){
                        
        //echo "User subscribed to room ".$room ."\n";

        $msg_to_broadcast = json_encode(['action'=>'newSubscriber', 'room'=>$room]);

        //notify user that someone has joined room
        foreach($this->rooms[$room] as $client){
            if ($client !== $from) {
                $client->send($msg_to_broadcast);
            }
        }
    }
    
    /*
    ********************************************************************************************************************************
    ********************************************************************************************************************************
    ********************************************************************************************************************************
    ********************************************************************************************************************************
    ********************************************************************************************************************************
    */
    
    private function notifyUsersOfDisconnection($room, $from){
        $msg_to_broadcast = json_encode(['action'=>'imOffline', 'room'=>$room]);

        //notify user that remote has left the room
        foreach($this->rooms[$room] as $client){
            if ($client !== $from) {
                $client->send($msg_to_broadcast);
            }
        }
    }
    // protected $clients;

    // public function __construct() {
    //     $this->clients = new \SplObjectStorage;
    //     echo 'connected';
    // }

    // public function onOpen(ConnectionInterface $conn) {
    //     // Store the new connection to send messages to later
    //     $this->clients->attach($conn);

    //     echo "New connection! ({$conn->resourceId})\n";
    // }

    // public function onMessage(ConnectionInterface $from, $msg) {        
    //     $numRecv = count($this->clients) - 1;
    //     echo sprintf('Connection %d sending message "%s" to %d other connection%s' . "\n"
    //         , $from->resourceId, $msg, $numRecv, $numRecv == 1 ? '' : 's');

    //     foreach ($this->clients as $client) {
    //         if ($from !== $client) {
    //             // The sender is not the receiver, send to each client connected
    //             $client->send($msg);
    //         }
    //     }
    // }

    // public function onClose(ConnectionInterface $conn) {
    //     // The connection is closed, remove it, as we can no longer send it messages
    //     $this->clients->detach($conn);

    //     echo "Connection {$conn->resourceId} has disconnected\n";
    // }

    // public function onError(ConnectionInterface $conn, \Exception $e) {
    //     echo "An error has occurred: {$e->getMessage()}\n";

    //     $conn->close();
    // }
}
?>