<?php 
session_start();
include_once dirname(__FILE__) .'/../../lib/common-admin.php';
if(isset($_POST) && !empty($_POST)) {
    $keyword = $_POST['searchText'];
    $getPeoples = "SELECT DISTINCT users.id, users.image, users.first_name, users.last_name, users.email, users.phone FROM users
                    INNER JOIN chat_room_members ON (chat_room_members.user_id = users.id) 
                    WHERE chat_room_members.room_id 
                    NOT IN (SELECT id FROM chat_room_members WHERE chat_room_members.user_id = '".$_SESSION['activeUserIdFront']."') 
                    AND users.is_active = ".true." AND users.is_verified = ".true." AND users.id != '".$_SESSION['activeUserIdFront']."'
                    AND (users.email LIKE '%".$keyword."%' or users.phone LIKE '%".$keyword."%' OR CONCAT_WS(users.first_name, users.last_name, ' ') LIKE '%".$keyword."%')
                    ORDER BY users.first_name ASC";
    $result = mysqli_query($dbConnection, $getPeoples);
    $numRows = mysqli_num_rows($result);
    if($numRows > 0) {
        $meta['responseCode'] = 200;
        $meta['data'] = [];
        $counter = 0;
        while ($chatRoomsArray = mysqli_fetch_assoc($result)) {
            $meta['data'][$counter]['contactId'] = base64_encode($chatRoomsArray['id']).'#10101#';
            $meta['data'][$counter]['contactImage'] = BASE_URL.'uploads/users/'.$chatRoomsArray['image'];
            $meta['data'][$counter]['contactName'] = $chatRoomsArray['first_name'].' '.$chatRoomsArray['last_name'];
            $meta['data'][$counter]['contactEmail'] = $chatRoomsArray['email'];
            $meta['data'][$counter]['contactPhone'] = $chatRoomsArray['phone'];
            $counter ++;
        }
    } else {
        $meta['responseCode'] = 400;
        $meta['data'] = [];
    }
} else {
    $meta['responseCode'] = 400;
    $meta['data'] = [];
}
echo json_encode($meta);
?>