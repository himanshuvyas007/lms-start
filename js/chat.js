var webSocket;
var allRooms = [];
var Controller = {};
var baseURL = window.location.host + '/lms_video';
// var webSocketUrl = `ws://${window.location.hostname}:5000`;

var handleSocketMessages = (message) => {
    console.log(message);
};


var sendWebsocketMessage = (message) => {
    message.room = Controller.globalVariables.currentChatRoom;
    message = JSON.stringify(message);
    webSocket.send(message);
}

// AJAX CALLING HELPER
var ajaxHelper = (options) => {
    // options.success = (data) => {
    //     successresults = data;
    // }
    
}

// API OBJECT
var API = {
    listAllChatrooms: () => {
        return new Promise(function(resolve, reject){
            let ajaxOptions = {
                url: 'http://' + baseURL + '/api/chatRoom/listAllChatrooms.php',
                cache: false,
                processData: false,
                type: 'get',
            }
            $.ajax(ajaxOptions).then(function(data) {
                console.log(data);
                return resolve(data);
            });
        });
    },
    getChatRoomDetails: (chatRoomID) => {
        let data = {
            chatRoomID: chatRoomID
        };
        let ajaxOptions = {
            url: 'http://' + baseURL + '/api/chatRoom/getChatRoomDetails.php',
            dataType: 'json',
            cache: false,
            contentType: 'application/json',
            processData: false,
            data: data,
            async: false,
            type: 'post',
        }
        let sampleData = {
            "chatRoomId": chatRoomID,
            "chatRoomOwner": "1234568",
            "chatRoomName": "tempChatRoom",
            "chatRoomImage": "https://www.cumbria.ac.uk/media/staff-profile-images/staff_profile_-generic_350x350px.png",
            "chatRoomType": "0",
            "chatMessages": [{
                    "chatMessageId": "12",
                    "sentBy": "123",
                    "message": "hey man whats upp?",
                    "file": false,
                    "date": "123"
                },
                {
                    "chatMessageId": "13",
                    "sentBy": "123",
                    "message": "hu",
                    "file": false,
                    "date": "123"
                },
                {
                    "chatMessageId": "14",
                    "sentBy": "321",
                    "message": "i dont know",
                    "file": false,
                    "date": "123"
                }
            ],
            "chatRoomCreatedDate": "123",
            "chatRoomMembers": [{
                "chatRoomMemberId": "123",
                "chatRoomMemberName": "Aakash",
            }, {
                "chatRoomMemberId": "321",
                "chatRoomMemberName": "himanshu",
            }],
            "isChatRoomActive": true
        }
        return sampleData;
        // return ajaxHelper(ajaxOptions);
    },
    uploadFile: (fileObject) => {
        let ajaxOptions = {
            url: 'http://' + baseURL + '/api/chatRoom/uploadFile.php',
            dataType: 'text',
            cache: false,
            async: false,
            contentType: false,
            processData: false,
            data: fileObject,
            type: 'post',
        }
        return ajaxHelper(ajaxOptions);
    },
    createGrpChatRoom: (chatRoomData) => {
        let ajaxOptions = {
            url: 'http://' + baseURL + '/api/chatRoom/createGrpChatRoom.php',
            dataType: 'json',
            cache: false,
            async: false,
            contentType: 'application/json',
            processData: false,
            data: chatRoomData,
            type: 'post',
        }
        return ajaxHelper(ajaxOptions);
    },
    createOne2OneChatRoom: (chatRoomData) => {
        let ajaxOptions = {
            url: 'http://' + baseURL + '/api/chatRoom/createOne2OneChatRoom.php',
            dataType: 'json',
            cache: false,
            async: false,
            contentType: 'application/json',
            processData: false,
            data: chatRoomData,
            type: 'post',
        }
        return ajaxHelper(ajaxOptions);
    },
    acceptChatRoomRequest: () => {
        let ajaxOptions = {
            url: 'http://' + baseURL + '/api/chatRoom/acceptChatRoomRequest.php',
            dataType: 'json',
            cache: false,
            async: false,
            contentType: 'application/json',
            processData: false,
            data: chatRoomData,
            type: 'post',
        }
        return ajaxHelper(ajaxOptions);
    },
    searchContacts: (searchData) => {
        let ajaxOptions = {
            url: 'http://' + baseURL + '/api/chatRoom/searchContacts.php',
            dataType: 'json',
            cache: false,
            async: false,
            contentType: 'application/json',
            processData: false,
            data: searchData,
            type: 'post',
        }
        return ajaxHelper(ajaxOptions);
    },
    getActiveUserId: (searchData) => {
        let ajaxOptions = {
            url: 'http://' + baseURL + '/api/users/getCurrentActiveUserId.php',
            dataType: 'json',
            cache: false,
            async: false,
            contentType: 'application/json',
            processData: false,
            data: searchData,
            type: 'post',
        }
        return ajaxHelper(ajaxOptions);
    }
};

var activeUserIdAPIResults = API.getActiveUserId();
Controller.globalVariables = {
    currentChatRoom: null,
    currentUserId: API.getActiveUserId()
};

var View = {
    getChatInstanseView: () => {
        return `<li class="cursor-pointer" >
        <div class="row align-items-center pt-3 pb-3" onclick="setChatRoom(this)" chatroom-id="{{chatRoomId}}" style="background:#f1f1f1;">
            <div class="col-3 text-center">
            <span class="badge unread"><span class="count">0</span></span>
                <img src="https://www.cumbria.ac.uk/media/staff-profile-images/staff_profile_-generic_350x350px.png" alt="Person" class="w-100 rounded-circle">
            </div>
            <div class="col pl-0 text-truncate">
                <h5 class="ft-poppins">{{name}}</h5>
                <p class="m-0 text-muted small">
                    Not available.
                </p>
            </div>
        </div>
        </li>`;
    },
    getRevieverInstanseView: () => {
        return `<div class="left">{{message}}</div>`;
    },
    getSenderInstanseView: () => {
        return `<div class="right">{{message}}</div>`;
    }
}

Controller.uploadFile = (file) => {
    let formData = new FormData();
    formData.append('file', file);
    // let uploadResult = true; // API.
    // if (uploadResult) {
    //     // SHOW THE FILE UPLOAD MESSAGE FROM SENDER

    // }
}


// SEARCH IN LISTED CONVERSATION
Controller.seacrhConversation = (searchText) => {
    let finalSeacrhList = [];
    if (allRooms.length > 0) {
        finalSeacrhList = allRooms.filter(function (chat) {
            if (chat.chatRoomName.includes(searchText)) {
                return chat;
            }
        });
    }
    return finalSeacrhList;
}


// LIST ALL PREVIOUS CONVERSATION
Controller.listAllChatrooms = (preListOfConversation = null) => {
    // let listAllChatrooms = API.listAllChatrooms();
    let listAllChatrooms;
    if (!preListOfConversation) {
        listAllChatrooms = [{
                chatRoomId: "abc",
                chatRoomName: "testRoom1"
            },
            {
                chatRoomId: "abc2",
                chatRoomName: "rtcRoom2"
            }
        ];
        allRooms = listAllChatrooms;
        Controller.globalVariables.currentChatRoom = listAllChatrooms[0].chatRoomName;
    } else {
        listAllChatrooms = preListOfConversation;
    }
    let chatHtmlInstance = View.getChatInstanseView();
    let finalRenderHtml = '';
    listAllChatrooms.map((chatRoom) => {
        let tempHtmlInstanse = chatHtmlInstance;
        tempHtmlInstanse = tempHtmlInstanse.toString().replace("{{name}}", chatRoom.chatRoomName);
        tempHtmlInstanse = tempHtmlInstanse.toString().replace("{{chatRoomId}}", chatRoom.chatRoomId);
        finalRenderHtml += tempHtmlInstanse;
    });
    return finalRenderHtml;
}


// MESSAGE SENDING FUNCTION
Controller.sendMessage = (message) => {
    let messagePayload = {
        from: '12345',
        message: message,
        createdAt: ((new Date().getTime()) / 1000),
    }
    // SEND MESSAGE TO SOCKET
    // sendWebsocketMessage(message);
    let senderHtmlInstanse = View.getSenderInstanseView();
    senderHtmlInstanse = senderHtmlInstanse.replace("{{message}}", message.toString());
    return senderHtmlInstanse;
}

// GET CHATROOM DETAILS 
Controller.getChatRoomDetails = (chatRoomId) => {
    let getChatRoomDetails = API.getChatRoomDetails(chatRoomId);
    let chats = getChatRoomDetails.chatMessages;
    let getRevieverInstanseView = View.getRevieverInstanseView();
    let getSenderInstanseView = View.getSenderInstanseView();
    let finalRenderHtml = '';
    let finalData = {
        chatRoomName: getChatRoomDetails.chatRoomName,
        chatRoomImage: getChatRoomDetails.chatRoomImage
    };
    chats.map((chat) => {
        if (chat.sentBy == Controller.globalVariables.currentUserId) {
            finalRenderHtml += getSenderInstanseView.replace("{{message}}", chat.message);
        } else {
            finalRenderHtml += getRevieverInstanseView.replace("{{message}}", chat.message);
        }
    });
    finalData.finalRenderHtml = finalRenderHtml
    return finalData;
}


var setupWebSocket = () => {
    webSocket = new WebSocket(webSocketUrl);
    webSocket.onopen(() => {
        console.log('Socket Server is connected');
    });
    webSocket.onmessage((message) => {
        handleSocketMessages(message);
    });
};
