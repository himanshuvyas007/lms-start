<?php
session_start();
include_once  dirname(__FILE__) .'/lib/common-admin.php';
if(isset($_POST) && !empty($_POST))
{
    try {
        // fetch user details by supplied email and password
        $check_user_for_login = "select id, email, is_active, is_verified from users where email = '".mysqli_real_escape_string($dbConnection, $_POST['email'])."' and password = '".md5($_POST['password'])."' and role_id != '".ADMINISTRATOR_ID."' ";
        $check_user_for_login_query = mysqli_query($dbConnection, $check_user_for_login);
        $check_user_for_login_result = mysqli_fetch_array($check_user_for_login_query, MYSQLI_ASSOC);
        if(!empty($check_user_for_login_result)) 
        {
            if($check_user_for_login_result['is_verified'] == true) 
            {
                if($check_user_for_login_result['is_active'] == true) 
                {
                    // check is remember is checked
                    if(isset($_POST['remember_me']) && $_POST['remember_me'] == '1') 
                    {
                        $_COOKIE['ccokie_login_email'] = $_POST['email'];
                        $_COOKIE['cookie_login_password'] = $_POST['password'];
                        $_COOKIE['cookie_login_remember_me'] = $_POST['remember_me'];
                    } else {
                        $_COOKIE['ccokie_login_email'] = "";
                        $_COOKIE['cookie_login_password'] = "";
                        $_COOKIE['cookie_login_remember_me'] = "";
                    }
                    $_SESSION['activeUserIdFront'] = $check_user_for_login_result['id'];
                    // update last login of admin
                    $update_last_login = "update users set last_login  = '".date("Y-m-d H:i:s")."' where id = '".$check_user_for_login_result['id']."'";
                    $update_last_login_query = mysqli_query($dbConnection, $update_last_login);
                    
                    // mark as success
                    echo json_encode(['status' => 200]);
                    die;
                } else {
                    // revert to login page
                    echo json_encode(['status' => 401, 'message' => "Your account has been deactivated by administrator"]);
                    die;
                } 
            } else {
                // revert to login page
                echo json_encode(['status' => 401, 'message' => "Your account has not been verified, please verify your email address from your inbox"]);
                die;
            }
        } else {
            // redirect to login page
            echo json_encode(['status' => 401, 'message' => "Invalid email address or password entered"]);
            die;
        }
    } catch (\Exception $e) {
        echo json_encode(['status' => 403, 'message' => "Something went wrong, please try again later"]);
        die;
    }
}