<?php session_start(); ?>
<?php ob_start(); ?>
<?php include_once '../lib/common-admin.php'; ?>
<?php redirect_to_dashboard(); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Forgot Password - <?php echo SITE_TITLE; ?></title>
        <!-- plugins:css -->
        <link rel="stylesheet" href="<?php echo BASE_ADMIN_URL ?>/css/materialdesignicons.min.css">
        <link rel="stylesheet" href="<?php echo BASE_ADMIN_URL ?>/css/vendor.bundle.base.css">
        <!-- endinject -->
        <!-- plugin css for this page -->
        <!-- End plugin css for this page -->
        <!-- inject:css -->
        <link rel="stylesheet" href="<?php echo BASE_ADMIN_URL ?>/css/style.css">
        <link rel="stylesheet" href="<?php echo BASE_ADMIN_URL ?>/css/custom.css">
        <!-- endinject -->
        <link rel="shortcut icon" href="<?php echo BASE_ADMIN_URL ?>/img/favicon.png" />
    </head>
    <body>
        <div class="container-scroller">
            <div class="container-fluid page-body-wrapper full-page-wrapper">
                <div class="content-wrapper d-flex align-items-center auth">
                    <div class="row w-100">
                        <div class="col-lg-4 mx-auto">
                            <div class="auth-form-light text-left p-5">
                                <?php include_once '../session_message.php'; ?>
                                <h4>Forgot Password</h4>
                                <form class="pt-3" action="<?php echo BASE_ADMIN_URL; ?>/check_forgot_password.php" method="post">
                                    <div class="form-group">
                                        <input name="email" type="email" class="form-control form-control-lg" id="exampleInputEmail1" placeholder="Email Address">
                                        <div class="error_message" id="error_email"></div>
                                    </div>
                                    <div class="mt-3">
                                        <button class="btn btn-block btn-gradient-primary btn-lg font-weight-medium auth-form-btn" type="submit">Submit</button>
                                    </div>
                                    <div class="my-2 d-flex justify-content-between align-items-center">
                                        <a href="<?php echo BASE_ADMIN_URL ?>" class="auth-link text-black">Back to Login</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- content-wrapper ends -->
            </div>
            <!-- page-body-wrapper ends -->
        </div>
        <!-- container-scroller -->
        <!-- plugins:js -->
        <script src="<?php echo BASE_ADMIN_URL ?>/js/vendor.bundle.base.js"></script>
        <script src="<?php echo BASE_ADMIN_URL ?>/js/vendor.bundle.addons.js"></script>
        <!-- endinject -->
        <!-- inject:js -->
        <script src="<?php echo BASE_ADMIN_URL ?>/js/off-canvas.js"></script>
        <script src="<?php echo BASE_ADMIN_URL ?>/js/misc.js"></script>
        <script src="<?php echo BASE_ADMIN_URL ?>/js/forgot_password.js"></script>
        <!-- endinject -->
    </body>

</html>
<?php ob_end_flush(); ?>