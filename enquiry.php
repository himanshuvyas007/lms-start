<?php
session_start();
include_once  dirname(__FILE__) .'/lib/common-admin.php';
use PHPMailer\PHPMailer\PHPMailer;
require dirname(__FILE__) .'/lib/vendor/autoload.php';
if(isset($_POST) && !empty($_POST))
{
    $validation_array = [];
    try {
        // validate all required fields

        if(!isset($_POST['name']) || $_POST['name'] == "")
            $validation_array['name'] = "Please enter your full name";

        if(!isset($_POST['email']) || $_POST['email'] == "")
            $validation_array['email'] = "Please enter your email address";

        if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
            $validation_array['email'] = "Please enter valid email address";

        if(!isset($_POST['message']) || $_POST['message'] == "")
            $validation_array['message'] = "Please enter your message";

        if(!isset($validation_array) || empty($validation_array)) 
        {
            $save_user = "insert into enquiries set name='".mysqli_real_escape_string($dbConnection, $_POST['name'])."', email='".$_POST['email']."', message='".mysqli_real_escape_string($dbConnection, $_POST['message'])."', created = '".date("Y-m-d H:i:s")."' ";
            $save_user_query = mysqli_query($dbConnection, $save_user);

            // mark as success
            echo json_encode(['status' => 200, 'message' => "Your enquiry has been submitted successfully"]);
            die;
        } else {
            echo json_encode(['status' => 401, 'message' => $validation_array]);
            die;
        }
    } catch (Exception $e) {
        echo json_encode(['status' => 403, 'message' => "Something went wrong, please try again later"]);
        die;
    }
}