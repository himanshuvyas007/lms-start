$(document).ready(function() {
    $("input[type=submit]").on("click", function() {
        if($.trim($("#old_password").val()) == "") 
        {
            $("#error_old_password").html("Please enter your old password");
            $("#old_password").focus();
            return false;
        } else {
            $("#error_old_password").html("");
        }
        if($.trim($("#new_password").val()) == "") 
        {
            $("#error_new_password").html("Please enter your new password");
            $("#new_password").focus();
            return false;
        } else {
            $("#error_new_password").html("");
        }
        console.log($("#confirm_change_password").val());
        if($.trim($("#confirm_change_password").val()) == "") 
        {
            $("#error_confirm_change_password").html("Please confirm your password");
            $("#confirm_change_password").focus();
            return false;
        } else {
            $("#error_confirm_change_password").html("");
        }
        if($.trim($("#confirm_change_password").val()) != $.trim($("#new_password").val())) 
        {
            $("#error_confirm_change_password").html("Passwords do not match");
            $("#confirm_change_password").focus();
            return false;
        } else {
            $("#error_confirm_change_password").html("");
        }
    });
})