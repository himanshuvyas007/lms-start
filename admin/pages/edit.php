<?php include_once dirname(__FILE__) . '/../header.php'; ?>
<script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo BASE_ADMIN_URL.'/js/pages/page.js'; ?>"></script>
<div class="page-header">
    <h3 class="page-title">
        Edit Page
    </h3>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Content Pages</a></li>
            <li class="breadcrumb-item"><a href="<?php echo BASE_ADMIN_URL ?>/users/lists.php">Pages List</a></li>
            <li class="breadcrumb-item active" aria-current="page">Edit Page</li>
        </ol>
    </nav>
</div>
<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <a href="<?php echo BASE_ADMIN_URL ?>/users/lists.php" class="btn btn-gradient-primary btn-fw">Pages List</a>
    </div>
</div>
<?php if(isset($validation_array['global']) && !empty($validation_array['global'])) { ?>
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card text-center">
            <div class="alert alert-danger"><?php echo $validation_array['global']; ?></div>
        </div>
    </div>
<?php } ?>
<div class="row">
    <?php 
    // fetch user details by id
    if(isset($_REQUEST['id']) && $_REQUEST['id'] != "")
    {
        $users_details = "select * from pages where id ='".$_REQUEST['id']."'";
        $users_details_query = mysqli_query($dbConnection, $users_details);
        $users_details_array = mysqli_fetch_array($users_details_query);
    } else {
        // redirect to listing page with exception
        $_SESSION['error'] = "Invalid request, please try again";
        redirect(BASE_ADMIN_URL.'/pages?page='.((isset($_REQUEST['page'])) ? $_REQUEST['page'] : 1));
    }
    if(isset($_POST) && !empty($_POST))
    {
        // save data
        $validation_array = [];
        try {

            // validate all required fields

            if(!isset($_POST['page_title']) || $_POST['page_title'] == "")
                $validation_array['page_title'] = "Please enter page title";

            if(!isset($_POST['content']) || $_POST['content'] == "")
                $validation_array['content'] = "Please enter content";
                
            // check that email address already exists or not
            $check_duplicate_email = "select id from pages where page_title = '".$_POST['page_title']."' and id != '".$_REQUEST['ID']."' ";
            $check_duplicate_email_query = mysqli_query($dbConnection, $check_duplicate_email);
            $check_duplicate_email_query_array = mysqli_fetch_array($check_duplicate_email_query);
            if(isset($check_duplicate_email_query_array['id'])) 
            {
                $validation_array['page_title'] = "Content Page with title already registered, please use different title";
            }

            if(!isset($validation_array) || empty($validation_array)) 
            {
                $save_user = "update pages set page_title='".mysqli_real_escape_string($dbConnection, $_POST['page_title'])."', content='".mysqli_real_escape_string($dbConnection, $_POST['content'])."', meta_title='".mysqli_real_escape_string($dbConnection, $_POST['meta_title'])."', meta_keywords='".mysqli_real_escape_string($dbConnection, $_POST['meta_keywords'])."', meta_description='".mysqli_real_escape_string($dbConnection, $_POST['meta_description'])."'
                             , modified='".date("Y-m-d H:i:s")."' where id  = '".$_REQUEST['id']."'";
                $save_user_query = mysqli_query($dbConnection, $save_user);
                $_SESSION['success'] = "Content page has been saved successfully";
                redirect(BASE_ADMIN_URL.'/pages?page='.$_REQUEST['page']);
            }            
        } catch (\Exception $e) {
            $validation_array['global'] = $e->getMessage();
        }
    }
    ?>
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <form class="forms-sample" action="" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="page_title">Page Title</label>
                        <input type="text" class="form-control" id="page_title" maxlength="100" name="page_title" value="<?php echo ((isset($_POST['page_title']) && $_POST['page_title'] != "") ? $_POST['page_title'] : $users_details_array['page_title']); ?>">
                        <div class="error_message" id="error_page_title"><?php echo ((isset($validation_array['page_title']) && !empty($validation_array['page_title'])) ? $validation_array['page_title'] : "");?></div>
                    </div>
                    <div class="form-group">
                        <label for="content">Content</label>
                        <textarea class="ckeditor" id="content" name="content"><?php echo ((isset($_POST['content']) && $_POST['content'] != "") ? $_POST['content'] : $users_details_array['content']); ?></textarea>
                        <div class="error_message" id="error_page_title"><?php echo ((isset($validation_array['content']) && !empty($validation_array['content'])) ? $validation_array['content'] : "");?></div>
                    </div>
                    <div class="form-group">
                        <label for="meta_title">Meta Title</label>
                        <input type="text" class="form-control" id="meta_title" name="meta_title" value="<?php echo ((isset($_POST['meta_title']) && $_POST['meta_title'] != "") ? $_POST['meta_title'] : $users_details_array['meta_title']); ?>">
                    </div>
                    <div class="form-group">
                        <label for="meta_keywords">Meta Keywords</label>
                        <input type="text" class="form-control" id="meta_keywords" name="meta_keywords" value="<?php echo ((isset($_POST['meta_keywords']) && $_POST['meta_keywords'] != "") ? $_POST['meta_keywords'] : $users_details_array['meta_keywords']); ?>">
                    </div>
                    <div class="form-group">
                        <label for="meta_description">Meta Description</label>
                        <input type="text" class="form-control" id="meta_description" name="meta_description"  value="<?php echo ((isset($_POST['meta_description']) && $_POST['meta_description'] != "") ? $_POST['meta_description'] : $users_details_array['meta_description']); ?>">
                    </div>
                    <button type="submit" class="btn btn-gradient-primary mr-2">Save Page</button>
                    <a href="<?php echo BASE_ADMIN_URL.'/pages'; ?>" class="btn btn-light">Cancel</a>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
<?php include_once dirname(__FILE__) . '/../footer.php'; ?>