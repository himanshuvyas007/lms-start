<?php session_start(); ?>
<?php ob_start(); ?>
<?php include_once dirname(__FILE__) .'/../lib/common-admin.php'; ?>
<?php redirect_to_login(); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title><?php echo SITE_TITLE; ?></title>
        <!-- plugins:css -->
        <link rel="stylesheet" href="<?php echo BASE_ADMIN_URL ?>/iconfonts/mdi/css/materialdesignicons.min.css">
        <link rel="stylesheet" href="<?php echo BASE_ADMIN_URL ?>/css/vendor.bundle.base.css">
        <!-- endinject -->
        <!-- inject:css -->
        <link rel="stylesheet" href="<?php echo BASE_ADMIN_URL ?>/css/style.css">
        <link rel="stylesheet" href="<?php echo BASE_ADMIN_URL ?>/css/custom.css">
        <!-- endinject -->
        <link rel="shortcut icon" href="<?php echo BASE_ADMIN_URL ?>/img/favicon.png" />
        
        <!-- plugins:js -->
        <script src="<?php echo BASE_ADMIN_URL ?>/js/vendor.bundle.base.js"></script>
        <script src="<?php echo BASE_ADMIN_URL ?>/js/vendor.bundle.addons.js"></script>
    </head>
    <body>
        <div class="container-scroller">
            <!-- partial:partials/_navbar.html -->
            <nav class="navbar default-layout-navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
                <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
                    <?php
                    $sql = "select site_logo from settings";
                    $sql_query = mysqli_query($dbConnection, $sql);
                    $sql_query_array = mysqli_fetch_array($sql_query); 
                    ?>
                    <a class="navbar-brand brand-logo" href="#">
                    <?php 
                        if(isset($sql_query_array['site_logo']) && file_exists(BASE_PATH.'uploads/settings/'.$sql_query_array['site_logo'])) 
                        {
                            echo '<img src="'.BASE_URL.'/uploads/settings/'.$sql_query_array['site_logo'].'" alt="logo"/>';
                        } else {
                            echo '<img src="'.BASE_ADMIN_URL.'/img/logo.svg" alt="logo"/>';
                        }
                    ?>
                    <a class="navbar-brand brand-logo-mini" href="#"><img src="<?php echo BASE_ADMIN_URL ?>/img/logo-mini.svg" alt="logo"/></a>
                </div>
                <div class="navbar-menu-wrapper d-flex align-items-stretch">
                    <ul class="navbar-nav navbar-nav-right">
                        <li class="nav-item nav-profile dropdown">
                            <a class="nav-link dropdown-toggle" id="profileDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
                                <div class="nav-profile-img">
                                    <img src="<?php echo BASE_ADMIN_URL ?>/img/faces/face1.jpg" alt="image">
                                    <span class="availability-status online"></span>             
                                </div>
                                <div class="nav-profile-text">
                                    <p class="mb-1 text-black">Admin</p>
                                </div>
                            </a>
                            <div class="dropdown-menu navbar-dropdown" aria-labelledby="profileDropdown">
                                <a class="dropdown-item" href="<?php echo BASE_ADMIN_URL ?>/edit_profile.php">
                                    <i class="mdi mdi-face-profile mr-2 text-success"></i>
                                    Edit Profile
                                </a>
                                <a class="dropdown-item" href="<?php echo BASE_ADMIN_URL ?>/change_password.php">
                                    <i class="mdi mdi-cached mr-2 text-success"></i>
                                    Change Password
                                </a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="<?php echo BASE_ADMIN_URL ?>/logout.php">
                                    <i class="mdi mdi-logout mr-2 text-primary"></i>
                                    Logout 
                                </a>
                            </div>
                        </li>
                    </ul>
                    <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
                        <span class="mdi mdi-menu"></span>
                    </button>
                </div>
            </nav>
            <!-- partial -->
            <div class="container-fluid page-body-wrapper">
                <?php include_once 'sidebar.php'; ?>
                <!-- partial -->
                <div class="main-panel">
                    <div class="content-wrapper">