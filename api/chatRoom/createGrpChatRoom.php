<?php 
session_start();
include_once dirname(__FILE__) .'/../../lib/common-admin.php';
//  API to create group communication channel
try {
    $data = [];
    if(isset($_POST) && !empty($_POST)) {
        $_SESSION['activeUserIdFront'] = 4;
        $groupMembers = $_POST['groupMembers'];
        $randomChatRoomName = $_POST['groupName'];
        if(!empty($groupMembers)) {
            // check for unique group name with user
            $checkUNiqueGroupUser = "select count(*) as cnt from chat_rooms where room_name = '".$randomChatRoomName."' and room_owner = '".$_SESSION['activeUserIdFront']."'";
            $checkUNiqueGroupUserQuery = mysqli_query($dbConnection, $checkUNiqueGroupUser);
            $checkUNiqueGroupUserArray = mysqli_fetch_assoc($checkUNiqueGroupUserQuery);
            if($checkUNiqueGroupUserArray['cnt'] == 0) {
                $createChannelQuery = "insert into chat_rooms set room_name = '".$randomChatRoomName."', room_owner = '".$_SESSION['activeUserIdFront']."', is_active = ".true.", 
                                    type = '".GROUP_CHAT_ROOM."', created = '".date("Y-m-d H:i:s")."', modified = '".date("Y-m-d H:i:s")."' ";
                $createChannelQueryResult = mysqli_query($dbConnection, $createChannelQuery);
                $chatRoomId = mysqli_insert_id($dbConnection);
                // add chat room member records
                foreach ($groupMembers as $userId) {
                    // decrypt user id
                    $userDecryptId = explode("#10101#", $userId);
                    $createChatRoomMembers = "insert into chat_room_members set room_id = '".$chatRoomId."', user_id = '".base64_decode($userDecryptId[0])."', is_user_active = 1, created = '".date("Y-m-d H:i:s")."', modified = '".date("Y-m-d H:i:s")."' ";
                    $createChatRoomMembersResult = mysqli_query($dbConnection, $createChatRoomMembers);
                }
                $meta['responseCode'] = 200;
            } else {
                $meta['responseCode'] = 403;
            }
        } else {
            $meta['responseCode'] = 400;
        }
    } else {
        throw new \Exception("Request data is not empty");
    }
} catch (\Exception $e) {
    $meta['responseCode'] = 400;
}
echo json_encode($meta);
?>