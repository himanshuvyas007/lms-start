<?php 
session_start();
include_once dirname(__FILE__) .'/../../lib/common-admin.php';
$_SESSION['activeUserIdFront'] = 4;
$query = "SELECT   
chat_rooms.id as chat_room_id,
(
	CASE 
	WHEN chat_rooms.type= ".ONE_TO_ONE_CHAT_ROOM."
	THEN 
	(
		SELECT CONCAT_WS(' ', u.first_name, u.last_name) FROM chat_room_members chr LEFT JOIN users AS u ON (chr.user_id = u.id) WHERE chr.user_id != '".$_SESSION['activeUserIdFront']."' AND chr.room_id = chat_rooms.id
	)
	ELSE
	chat_rooms.room_name
	END
) AS room_name
FROM chat_rooms INNER JOIN chat_room_members ON chat_rooms.id = chat_room_members.room_id WHERE chat_room_members.is_user_active = ".true." AND chat_room_members.user_id = '".$_SESSION['activeUserIdFront']."'";
$result = mysqli_query($dbConnection, $query);
$numRows = mysqli_num_rows($result);
if($numRows > 0) {
    $meta['responseCode'] = 200;
    $meta['data'] = [];
    $counter = 0;
    while ($chatRoomsArray = mysqli_fetch_assoc($result)) {
        $meta['data'][$counter]['chatRoomId'] = base64_encode($chatRoomsArray['chat_room_id']).'#10101#';
        $meta['data'][$counter]['chatRoomName'] = $chatRoomsArray['room_name'];
        $counter ++;
    }
} else {
    $meta['responseCode'] = 400;
    $meta['data'] = [];
}
echo json_encode($meta);
?>