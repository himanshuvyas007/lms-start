$(document).ready(function() {
    $("button[type=submit]").on("click", function() {
        if($.trim($("#site_title").val()) == "")
        {
            $("#error_site_title").html("Please enter site title");
            $("#site_title").focus();
            return false;
        } else {
            $("#error_site_title").html("");
        }
        if($.trim($("#site_email").val()) == "")
        {
            $("#error_site_email").html("Please enter title of the site");
            $("#site_email").focus();
            return false;
        } else {
            $("#error_site_email").html("");
        }
        
        if($.trim($("#site_email").val()) != "")
        {
            var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
            if (reg.test($("#site_email").val()) == false)
            {

                $("#error_site_email").html("Please enter valid email address");
                $("#site_email").focus();
                return false;
            } else {
                $("#error_site_email").html("");
            }
        }
        if($.trim($("#contact_email").val()) != "")
        {
            var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
            if (reg.test($("#contact_email").val()) == false)
            {

                $("#error_contact_email").html("Please enter valid email address");
                $("#contact_email").focus();
                return false;
            } else {
                $("#error_contact_email").html("");
            }
        }
        if($.trim($("#smtp_port").val()) != "" && isNaN($("#smtp_port").val()))
        {
            $("#error_smtp_port").html("Please enter only numeric values");
            $("#smtp_port").focus();
            return false;
        } else {
            $("#error_smtp_port").html("");
        }
        if($.trim($("#rows_per_page").val()) == "")
        {
            $("#error_rows_per_page").html("Please enter records per page for listing");
            $("#rows_per_page").focus();
            return false;
        } else {
            $("#error_rows_per_page").html("");
        }
        if($.trim($("#rows_per_page").val()) != "" && isNaN($("#rows_per_page").val()))
        {
            $("#error_rows_per_page").html("Please enter only numeric values");
            $("#rows_per_page").focus();
            return false;
        } else {
            $("#error_rows_per_page").html("");
        }
    });
})