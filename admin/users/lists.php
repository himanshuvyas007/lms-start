<?php include_once dirname(__FILE__) .'/../header.php'; ?>
<?php $siteSettings = getSettings(); ?>
<?php include_once dirname(__FILE__) . '/../../lib/paginator.php'; ?>
<style type="text/css" href="<?php echo BASE_URL.'/css/pagination.css'; ?>"></style>
<div class="page-header">
    <h3 class="page-title">
        Users Management
    </h3>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Users Management</a></li>
            <li class="breadcrumb-item active" aria-current="page">Users List</li>
        </ol>
    </nav>
</div>
<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <a href="<?php echo BASE_ADMIN_URL ?>/users/add.php" class="btn btn-gradient-primary btn-fw">Add User</a>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
       <?php include_once BASE_PATH.'/session_message.php'; ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>SL No</th>
                            <th>#</th>
                            <th>Name</th>
                            <th>Email Address</th>
                            <th>Status</th>
                            <th>Verified</th>
                            <th>Date</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $pages = new Paginator($siteSettings['rows_per_page'],'p');
                        $page_no = ((isset($_REQUEST['page']) && $_REQUEST['page'] != "") ? $_REQUEST['page'] : 1);
                        $offset = ($page_no - 1) * ROWS_PER_PAGE;
                        $users_lists = "select u.id, u.first_name, u.last_name, u.email, u.image, u.is_active, u.is_verified, u.created, r.name as role_name from users u left join roles r on (u.role_id = r.id) where r.key_name !=  'ROLE_ADMINISTRATOR' and u.is_deleted = 0 ";
                        $users_pagination_query = mysqli_query($dbConnection, $users_lists);
                        $users_pagination_count = mysqli_num_rows($users_pagination_query);
                        $pages->set_total($users_pagination_count);
                        $users_lists_limit = $users_lists. $pages->get_limit();
                        $users_lists_query = mysqli_query($dbConnection, $users_lists_limit);
                        $users_lists_num_rows = mysqli_num_rows($users_lists_query);
                        if($users_lists_num_rows > 0) {
                            $counter = 1;
                            while($users_lists_array = mysqli_fetch_array($users_lists_query)) { ?>
                            <tr>
                                <td><?php echo $counter + $offset; ?></td>
                                <td>
                                    <?php if(isset($users_lists_array['image']) && $users_lists_array['image'] != "" && file_exists(BASE_PATH.'/uploads/users/'.$users_lists_array['image'])) { ?>
                                        <img src="<?php echo BASE_URL.'/uploads/users/'.$users_lists_array['image']; ?>" width="100">
                                    <?php } else { ?>
                                        <img src="<?php echo BASE_ADMIN_URL.'/img/no-available-image.png'; ?>" width="100">
                                    <?php }?>
                                </td>
                                <td><?php echo $users_lists_array['first_name'].' '.$users_lists_array['last_name']; ?></td>
                                <td><a href="mailto:<?php echo $users_lists_array['email']; ?>"><?php echo $users_lists_array['email']; ?></a></td>
                                <td>
                                    <?php if($users_lists_array['is_active'] == '1') { ?>
                                        <label class="badge badge-info">Active</label>
                                    <?php } else { ?>
                                        <label class="badge badge-danger">In Active</label>
                                    <?php }?>
                                </td>
                                <td>
                                    <?php if($users_lists_array['is_verified'] == 1) { ?>
                                        <i class="mdi mdi-check"></i> 
                                    <?php } else { ?>
                                        <i class="mdi mdi-window-close"></i>
                                    <?php }?>
                                </td>
                                <td><?php echo date("M d, Y H:i A", strtotime($users_lists_array['created'])); ?></td>
                                <td>  
                                    <?php if($users_lists_array['is_active'] == '1') { ?>
                                        <a href="<?php echo BASE_ADMIN_URL.'/users/change_status.php?id='.$users_lists_array['id'].'&   =0&page='.$page_no ?>" title="Deactivate User"><i class="mdi mdi-close icon-sm"></i></a>
                                    <?php } else { ?>
                                        <a href="<?php echo BASE_ADMIN_URL.'/users/change_status.php?id='.$users_lists_array['id'].'&status=1&page='.$page_no ?>" title="Activate User"><i class="mdi mdi-check icon-sm"></i></a>
                                    <?php }?>
                                    <a href="<?php echo BASE_ADMIN_URL.'/users/edit.php?id='.$users_lists_array['id'].'&page='.$page_no ?>" title="Edit User"><i class="mdi mdi-pencil icon-sm"></i></a>
                                    <a href="<?php echo BASE_ADMIN_URL.'/users/delete.php?id='.$users_lists_array['id'].'&page='.$page_no ?>" title="Delete User" onclick="return confirm('Are you sure you want to delete this user?');"><i class="mdi mdi-delete icon-sm"></i></a>
                                </td>
                            </tr>
                        <?php 
                            $counter = $counter + 1;
                        } 
                        ?>
                        <tr>
                            <td colspan="8" align="center"><?php echo $pages->page_links(); ?></td>
                        </tr>
                        <?php } else { ?>
                        <tr>
                            <td colspan="8" align="center">No users registered</td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
<?php include_once dirname(__FILE__) .'/../footer.php'; ?>