$(document).ready(function() {
    $("button[type=submit]").on("click", function() {
        if($.trim($("#exampleInputEmail1").val()) == "") 
        {
            $("#error_email").html("Please enter your email address");
            $("#exampleInputEmail1").focus();
            return false;
        } else {
            $("#error_email").html("");
        }
    });
})