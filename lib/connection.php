<?php
include_once dirname(__FILE__) .'/../constants.php';

if(!$dbConnection = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD))
{
    die("Error connecting to the mysql server");
}

if(!mysqli_select_db($dbConnection, DB_NAME))
{
    die("Error connecting to the database");
}


