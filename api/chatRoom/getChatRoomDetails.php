<?php 
session_start();
include_once dirname(__FILE__) .'/../../lib/common-admin.php';
if(isset($_POST) && !empty($_POST)) {
    $roomId = $_POST['chatRoomID'];
    $query = "SELECT chat_rooms.*, chats.messages FROM chat_rooms 
    LEFT JOIN chats ON chats.room_id = chat_rooms.id 
    LEFT JOIN chat_room_members ON chat_room_members.room_id = chat_rooms.id
    WHERE chat_rooms.id = '".$roomId."' ";
    $result = mysqli_query($dbConnection, $query);
    $numRows = mysqli_num_rows($result);
    if($numRows > 0) {
        $meta['responseCode'] = 200;
        $meta['data'] = [];
        $counter = 0;
        $chatRoomsArray = mysqli_fetch_assoc($result);
        $meta['data']['chatRoomId'] = base64_encode($chatRoomsArray['id']).'#10101#';
        $meta['data']['chatRoomOwner'] = $chatRoomsArray['room_owner'];
        $meta['data']['chatRoomName'] = $chatRoomsArray['room_name'];
        $meta['data']['chatRoomImage'] = BASE_URL .'uploads/chatRooms/'.$chatRoomsArray['image'];
        $meta['data']['chatRoomType'] = $chatRoomsArray['type'];
        $meta['data']['chatMessages'] = ((isset($chatRoomsArray['messages'])) ? json_decode($chatRoomsArray['messages']) : []);
        $meta['data']['chatRoomCreatedDate'] = strtotime($chatRoomsArray['created']);
        $meta['data']['isChatRoomActive'] = $chatRoomsArray['is_active'];

        // load active members within chat room
        $getChatActiveMembers = "select chat_room_members.user_id, users.first_name, users.last_name from chat_room_members left join users on (chat_room_members.user_id = users.id) where chat_room_members.room_id = '".$roomId."' and chat_room_members.is_user_active = 1 ";
        $getChatActiveMembersQuery = mysqli_query($dbConnection, $getChatActiveMembers);
        $counter = 0;
        while ($getChatActiveMembersArray = mysqli_fetch_assoc($getChatActiveMembersQuery)) {
            $meta['data']['chatRoomMembers'][$counter]['chatRoomMemberId'] = $getChatActiveMembersArray['user_id'];
            $meta['data']['chatRoomMembers'][$counter]['chatRoomMemberName'] = $getChatActiveMembersArray['first_name'].' '. $getChatActiveMembersArray['last_name'];
            $counter++;
        }
    } else {
        $meta['responseCode'] = 400;
        $meta['data'] = [];
    }
}
echo json_encode($meta);
?>