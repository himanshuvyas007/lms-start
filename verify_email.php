<?php session_start(); ?>
<?php ob_start(); ?>
<?php include_once dirname(__FILE__) .'/lib/common-admin.php'; ?>
<?php include_once 'header.php'; ?>
<div class="maindiv">
    <?php 
       if(isset($_REQUEST['key']) && $_REQUEST['key'] != "" && isset($_REQUEST['email']) && $_REQUEST['email'] != "")
       {
           // check email address exist or not
           $check_email_exist = "select id, is_verified from users where email = '".$_REQUEST['email']."'";
           $check_email_exist_query = mysqli_query($dbConnection, $check_email_exist);
           $check_email_exist_array = mysqli_fetch_array($check_email_exist_query);
           if(!empty($check_email_exist_array))
           {
               if($check_email_exist_array['is_verified'] == false)
               {
                   // check valid key email pair
                   $check_valid_key = "select id, email from users where verify_email_address_key = '".$_REQUEST['key']."' and email = '".$_REQUEST['email']."' ";
                   $check_valid_key_query = mysqli_query($dbConnection, $check_valid_key);
                   $check_valid_key_array = mysqli_fetch_array($check_valid_key_query);
                   if(!empty($check_valid_key_array))
                   {
                        $update_key = "update users set is_verified =1, verify_email_address_key = null where email = '".$_REQUEST['email']."'";
                        $update_key_query = mysqli_query($dbConnection, $update_key);
                        echo '<div class="alert alert-success text-center">Your email address has been verified successfully, please login to continue</div>';
                   } else {
                       echo '<div class="alert alert-danger text-center">Either key is not valid or not associated with supplied email</div>';
                   }
               } else {
                   echo '<div class="alert alert-danger text-center">Your email address has already been verified.</div>';
               }
           } else {
               echo '<div class="alert alert-danger text-center">Email address does not exist</div>';
           }
       } else {
           echo '<div class="alert alert-danger text-center">Invalid request, please try again/div>';
       }
    ?>
</div>
<?php include_once 'footer.php'; ?>
<?php ob_end_flush(); ?>